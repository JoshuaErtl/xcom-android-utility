#include "FileWriter.h"


FileWriter::FileWriter(char const* const& filename):
	_output(filename){}

FileWriter::~FileWriter(){
	_output.close();
}


FileWriter& FileWriter::end(){
	_output << std::endl;
}

FileWriter& FileWriter::indent(uint8_t amount){
	while(amount > 0){
		_output << '\t';
		amount--;
	}
	return *this;
}

FileWriter& FileWriter::write(std::string const& value){
	_output << value.c_str();
	return *this;
}