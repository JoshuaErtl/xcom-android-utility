#pragma once

#include <fstream>


class FileWriter{
	private:
		/*
			Output file to write to.
		*/
		std::ofstream _output;


	public:
		/*
			Creates a writer for a file.

			\param 'filename' Name of the file to write.
		*/
		FileWriter(char const* const& filename);

		~FileWriter();


		/*
			Writes a new line to the file.

			\return This writer.
		*/
		FileWriter& end();

		/*
			Writes a number of indents to the file.

			\param 'amount' Number of indents.

			\return This writer.
		*/
		FileWriter& indent(uint8_t amount);

		/*
			Writes a string value to output.

			\param 'value' String value to write from.

			\return This writer.
		*/
		FileWriter& write(std::string const& value);
};