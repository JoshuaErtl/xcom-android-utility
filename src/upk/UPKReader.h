#pragma once

#include <fstream>


class UPKReader{
	private:
		/*
			Reads data from input file.

			\param 'dest' Destination to read into.
			\param 'size' Byte size of data to read.
		*/
		virtual void const read(void* const& dest, std::streamsize const& size) = 0;


	protected:
		/*
			Index of current read position.
		*/
		int32_t _index = 0;


	public:
		/*
			Gets the current read offset.

			\return Current read offset.
		*/
		virtual int32_t const getOffset() const = 0;


		/*
			Sets the read offset.

			\param 'offset' New read offset.

			\return Previous read offset.
		*/
		virtual int32_t const setOffset(int32_t const& offset) = 0;


		/*
			Reads a 32 bit float value from input.

			\param 'value' 32 bit float value to read into.
		*/
		void const read(float& value);

		/*
			Reads an 8 bit value from input.

			\param 'value' 8 bit value to read into.
		*/
		void const read(int8_t& value);
		/*
			Reads an 8 bit unsigned value from input.

			\param 'value' 8 bit unsigned value to read into.
		*/
		void const read(uint8_t& value);

		/*
			Reads a 16 bit value from input.

			\param 'value' 16 bit value to read into.
		*/
		void const read(int16_t& value);
		/*
			Reads a 16 bit unsigned value from input.

			\param 'value' 16 bit unsigned value to read into.
		*/
		void const read(uint16_t& value);

		/*
			Reads a 32 bit value from input.

			\param 'value' 32 bit value to read into.
		*/
		void const read(int32_t& value);
		/*
			Reads a 32 bit unsigned value from input.

			\param 'value' 32 bit unsigned value to read into.
		*/
		void const read(uint32_t& value);

		/*
			Reads a 64 bit value from input.

			\param 'value' 64 bit value to read into.
		*/
		void const read(int64_t& value);

		/*
			Reads an ASCII null terminated string value from input.

			\param 'length' Length of the string including null byte.

			\return String value.
		*/
		char const* const readString(int32_t const& length);


		/*
			Converts a chunk of memory into a hexidecimal string.

			\param 'source' Source to read from.
			\param 'size' Byte size of data to read.
			\param 'prefix' 'true' to add hexadecimal prefix.

			\return Hexidecimal string.
		*/
		static std::string const toHexString(void const* const& source, std::streamsize const& size, bool const& prefix = true);
};