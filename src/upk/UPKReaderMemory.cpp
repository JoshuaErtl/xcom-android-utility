#include "UPKReaderMemory.h"

#include <string>


UPKReaderMemory::UPKReaderMemory(uint8_t const* const& data, int32_t const& size, int32_t const& offset):
	_data(data),
	_offset(offset),
	_size(size){}


int32_t const UPKReaderMemory::getOffset() const{
	return _offset + _index;
}


int32_t const UPKReaderMemory::setOffset(int32_t const& offset){
	int32_t const previousIndex = _index + _offset;
	_index = offset - _offset;
	return previousIndex;
}


void const UPKReaderMemory::read(void* const& dest, std::streamsize const& size){
	if(_index < 0 || _index + size > _size){
		throw std::runtime_error(std::string("UPKReaderMemory::read(..., ").append(std::to_string(size)).append(") : Attempting to read outside data range!\n\t(Read Size: ").append(std::to_string(size)).append("  Index: ").append(std::to_string(_index)).append("  Size: ").append(std::to_string(_size)).append(")"));
	}
	char* const value = (char*) dest;
	for(int32_t i = 0; i < size; i++){
		value[i] = _data[_index++];
	}
}