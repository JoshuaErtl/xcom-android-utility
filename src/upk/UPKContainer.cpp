#include "UPKContainer.h"

#include "UPKReaderFile.h"


UPKContainer::UPKContainer(char const* const& filename){
	UPKReaderFile input(filename);
	_summary = new FPackageFileSummary(input);
	input.outputUnused();
}

UPKContainer::~UPKContainer(){
	delete _summary;
}


void const UPKContainer::write(char const* const& filename) const{
	UPKWriter output(filename);
	_summary->write(output, true);
}