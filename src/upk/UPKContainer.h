#pragma once

#include "contents/FPackageFileSummary.h"


class UPKContainer{
	private:
		/*
			Summary of UPK file.
		*/
		FPackageFileSummary* _summary;


	public:
		/*
			Creates a container for a UPK file.

			\param 'filename' Name of the UPK file to read.
		*/
		UPKContainer(char const* const& filename);

		~UPKContainer();


		/*
			Writes the container to a UPK file.

			\param 'filename' Name of the UPK file to write.
		*/
		void const write(char const* const& filename) const;
};