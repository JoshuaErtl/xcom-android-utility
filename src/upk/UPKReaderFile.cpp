#include "UPKReaderFile.h"

#include <iomanip>
#include <sstream>


UPKReaderFile::UPKReaderFile(char const* const& filename):
	_input(filename, std::ios_base::binary){
	{
		std::streamoff const size = _input.seekg(0, std::ios_base::end).tellg();
		if(size <= 0 || size > UINT_MAX){
			throw std::runtime_error(std::string("UPKReaderFile::UPKReaderFile(\"").append(filename).append("\") : File size out of bounds!\n\t(0 < ").append(std::to_string(size).append(" <= ")).append(std::to_string(UINT_MAX)).append(")"));
		}
		_input.seekg(0, std::ios_base::beg);
		_size = (int32_t) size;
	}
	{
		int32_t const size = (_size / 8) + 1;
		_used = new uint_least8_t[size];
		for(int32_t i = 0; i < size; i++){
			_used[i] = 0;
		}
	}
	{
		size_t const size = strlen(filename) + 1;
		_filename = new char[size];
		if(strcpy_s(_filename, size, filename)){
			throw std::runtime_error(std::string("UPKReaderFile::UPKReaderFile(\"").append(filename).append("\") : Filename failed to copy!"));
		}
	}
}

UPKReaderFile::~UPKReaderFile(){
	delete[] _filename;
	_input.close();
	delete[] _used;
}


int32_t const UPKReaderFile::getOffset() const{
	return _index;
}


int32_t const UPKReaderFile::setOffset(int32_t const& offset){
	int32_t const previousIndex = _index;
	_index = offset;
	_input.seekg(offset, std::ios_base::beg);
	return previousIndex;
}


void const UPKReaderFile::outputUnused(){
	{
		bool allUsed = true;
		for(int32_t i = 0; i < _size; i++){
			if((_used[i >> 3] & (0x1 << (i & 0x7))) == 0){
				allUsed = false;
				break;
			}
		}
		if(allUsed){
			return;
		}
	}
	std::streampos prevPos = _input.tellg();
	_input.seekg(0, std::ios_base::beg);
	int32_t column;
	int32_t index = 0;
	std::ofstream output(std::string(_filename).append(" Unused.txt"));
	bool readPrevious = true;
	int16_t temp = 0;
	bool writeLine;
	bool wrotePrevious = true;
	while(index < _size){
		std::stringstream text;
		text << std::hex << std::uppercase << std::setfill('0') << std::setw(8) << index << '\t';
		writeLine = false;
		for(column = 0; column < 16 && index < _size; column++, index++){
			if(column > 0){
				text << ' ';
			}
			if((_used[index >> 3] & (0x1 << (index & 0x7))) > 0){
				readPrevious = false;
				text << ' ' << ' ';
			}
			else{
				if(!readPrevious){
					_input.seekg(index, std::ios_base::beg);
				}
				if(_input.read((char*) &temp, 1).good()){
					readPrevious = true;
					text << std::setfill('0') << std::setw(2) << temp;
					writeLine = true;
				}
				else{
					throw std::runtime_error(std::string("UPKReader::outputUnused() : Could not read input file!\n\t(Index: ").append(toHexString(&index, sizeof(index))).append(")"));
				}
			}
		}
		if(writeLine){
			if(!wrotePrevious){
				output << "..." << std::endl;
			}
			output << text.str() << std::endl;;
		}
		wrotePrevious = writeLine;
	}
	_input.seekg(prevPos, std::ios_base::beg);
}

void const UPKReaderFile::read(void* const& dest, std::streamsize const& size){
	if(!_input.read((char*) dest, size).good()){
		throw std::runtime_error(std::string("UPKReaderFile::read(..., ").append(std::to_string(size)).append(") : Failed to read!\n\t(Index: ").append(toHexString(&_index, sizeof(_index))).append(")"));
	}
	uint_least8_t mark;
	for(std::streamsize _ = 0; _ < size; _++){
		if(_index > _size){
			throw std::runtime_error(std::string("UPKReaderFile::read(..., ").append(std::to_string(size)).append(") : Attempted to read out of file!"));
		}
		uint_least8_t& value = _used[_index >> 3];
		mark = 0x1 << (_index & 0x7);
		if((value & mark) > 0){
			throw std::runtime_error(std::string("UPKReaderFile::read(..., ").append(std::to_string(size)).append(") : Attempted to read previously read contents!\n\t(Index: ").append(toHexString(&_index, sizeof(_index))).append(")"));
		}
		value |= mark;
		_index++;
	}
}