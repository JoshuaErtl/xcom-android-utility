#include "UPKWriter.h"

#include <string>

#include "UPKReader.h"


UPKWriter::UPKWriter(char const* const& filename):
	_output(filename, std::ios_base::binary){}

UPKWriter::~UPKWriter(){
	_output.close();
}


void const UPKWriter::write(float const& value){
	write(&value, 4);
}

void const UPKWriter::write(int8_t const& value){
	write(&value, 1);
}
void const UPKWriter::write(uint8_t const& value){
	write(&value, 1);
}

void const UPKWriter::write(int16_t const& value){
	write(&value, 2);
}
void const UPKWriter::write(uint16_t const& value){
	write(&value, 2);
}

void const UPKWriter::write(int32_t const& value){
	write(&value, 4);
}
void const UPKWriter::write(uint32_t const& value){
	write(&value, 4);
}

void const UPKWriter::write(int64_t const& value){
	write(&value, 8);
}

void const UPKWriter::writeString(char const* const& value){
	write(value, strlen(value) + 1);
}


void const UPKWriter::write(void const* const& src, std::streamsize const& size){
	if(!_output.write((char*) src, size).good()){
		throw std::runtime_error(std::string("UPKWriter::write(..., ").append(std::to_string(size)).append(") : Failed to write!\n\t(Index: ").append(UPKReader::toHexString(&_output.tellp(), sizeof(std::streampos))).append(")"));
	}
}