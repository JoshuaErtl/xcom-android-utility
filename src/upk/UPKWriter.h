#pragma once

#include <fstream>


class UPKWriter{
	private:
		/*
			Output file to write to.
		*/
		std::ofstream _output;


		/*
			Writes data to output file.

			\param 'src' Source to write from.
			\param 'size' Byte size of data to write.
		*/
		void const write(void const* const& src, std::streamsize const& size);


	public:
		/*
			Creates a writer for a UPK file.

			\param 'filename' Name of the UPK file to write.
		*/
		UPKWriter(char const* const& filename);

		~UPKWriter();


		/*
			Writes a 32 bit float value to output.

			\param 'value' 32 bit float value to write from.
		*/
		void const write(float const& value);

		/*
			Writes an 8 bit value to output.

			\param 'value' 8 bit value to write from.
		*/
		void const write(int8_t const& value);
		/*
			Writes an 8 bit unsigned value to output.

			\param 'value' 8 bit unsigned value to write from.
		*/
		void const write(uint8_t const& value);

		/*
			Writes a 16 bit value to output.

			\param 'value' 16 bit value to write from.
		*/
		void const write(int16_t const& value);
		/*
			Writes a 16 bit unsigned value to output.

			\param 'value' 16 bit unsigned value to write from.
		*/
		void const write(uint16_t const& value);

		/*
			Writes a 32 bit value to output.

			\param 'value' 32 bit value to write from.
		*/
		void const write(int32_t const& value);
		/*
			Writes a 32 bit unsigned value to output.

			\param 'value' 32 bit unsigned value to write from.
		*/
		void const write(uint32_t const& value);

		/*
			Writes a 64 bit value to output.

			\param 'value' 64 bit value to write from.
		*/
		void const write(int64_t const& value);

		/*
			Writes an ASCII null terminated value to output.

			\param 'value' String value.
		*/
		void const writeString(char const* const& value);
};