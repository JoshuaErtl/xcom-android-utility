#pragma once

#include "UPKReader.h"


class UPKReaderFile : public UPKReader{
	private:
		/*
			Input file to read from.
		*/
		std::ifstream _input;


		/*
			Name of input file.
		*/
		char* _filename;

		/*
			Size of input file.
		*/
		int32_t _size;

		/*
			Bit mapping where '(_used[_index >> 3] & (0x1 << (_index & 0x7)) > 0'
			means a byte has been read.
		*/
		uint_least8_t* _used;


		void const read(void* const& dest, std::streamsize const& size) override;


	public:
		/*
			Creates a reader for a UPK file.

			\param 'filename' Name of the UPK file to read.
		*/
		UPKReaderFile(char const* const& filename);

		~UPKReaderFile();


		int32_t const getOffset() const override;


		int32_t const setOffset(int32_t const& offset) override;


		/*
			Outputs the unused values to a debug file.
		*/
		void const outputUnused();
};