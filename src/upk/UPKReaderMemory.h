#pragma once

#include "UPKReader.h"


class UPKReaderMemory : public UPKReader{
	private:
		/*
			Data to read from.
		*/
		uint8_t const* const& _data;

		/*
			 Offset in file '_data' would represent.
		*/
		int32_t const& _offset;

		/*
			Size of '_data'.
		*/
		int32_t const& _size;


		void const read(void* const& dest, std::streamsize const& size) override;


	public:
		/*
			Creates a reader for UPK data stored in memory.

			\param 'data' Data to read from.
			\param 'size' Size of 'data'.
			\param 'offset' Offset in file 'data' would represent.
		*/
		UPKReaderMemory(uint8_t const* const& data, int32_t const& size, int32_t const& offset = 0);


		int32_t const getOffset() const override;


		int32_t const setOffset(int32_t const& offset) override;
};