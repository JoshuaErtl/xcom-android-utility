#pragma once

#include <vector>

#include "../Object.h"


template<class T> class TArray : public Object{
	private:
		/*
			Contained array.
		*/
		std::vector<T*> _array;


	public:
		/*
			Creates an array of objects.

			\param 'input' Input UPK file reader.
		*/
		TArray(UPKReader& input){
			int32_t size;
			input.read(size);
			_array.resize(size);
			for(int32_t i = 0; i < size; i++){
				_array.at(i) = new T(input);
			}
		}

		~TArray(){
			for(typename std::vector<T*>::const_iterator it = _array.begin(); it != _array.end(); it++){
				delete *it;
			}
		}


		/*
			Gets the number of elements in the array.

			\return Number of elements in the array.
		*/
		int32_t const elements() const{
			return _array.size();
		}

		/*
			Gets an object entry.

			\param 'index' Index of the entry.

			\return Object entry.
		*/
		T const* const get(int32_t const& index) const{
			return _array.at(index);
		}

		uint32_t const size() const override{
			uint32_t size = sizeof(int32_t);
			for(typename std::vector<T*>::const_iterator it = _array.begin(); it != _array.end(); it++){
				size += (*it)->size();
			}
			return size;
		}


		void const write(UPKWriter& output) const override{
			output.write((int32_t) _array.size());
			for(typename std::vector<T*>::const_iterator it = _array.begin(); it != _array.end(); it++){
				(*it)->write(output);
			}
		}
};