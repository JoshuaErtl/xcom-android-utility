#pragma once

#include "../Object.h"


template<class TK, class TI> class TPair : public Object{
	private:
		/*
			Key of the pairing.
		*/
		TK* _key;

		/*
			Value of the pairing.
		*/
		TI* _value;


	public:
		/*
			Creates a pairing of key and value.

			\param 'input' Input UPK file reader.
		*/
		TPair(UPKReader& input){
			_key = new TK(input);
			_value = new TI(input);
		}

		~TPair(){
			delete _key;
			delete _value;
		}


		/*
			Gets the key of the pair.

			\return Key of the pair.
		*/
		TK const* const key() const{
			return _key;
		}

		uint32_t const size() const override{
			return _key->size() + _value->size();
		}

		/*
			Gets the value of the pair.

			\return Value of the pair.
		*/
		TK const* const value() const{
			return _value;
		}


		void const write(UPKWriter& output) const override{
			_key->write(output);
			_value->write(output);
		}
};