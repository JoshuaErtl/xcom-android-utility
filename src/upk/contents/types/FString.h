#pragma once

#include "../Object.h"


class FString : public Object{
	private:
		/*
			Contained string.
		*/
		char const* _string = nullptr;


	public:
		/*
			Creates a null terminated string.

			\param 'input' Input UPK file reader.
		*/
		FString(UPKReader& input);

		~FString();


		uint32_t const size() const override;


		/*
			Checks if the string is equal to another string.

			\param 's' String to check.

			\return 'true' if strings are equal.
		*/
		bool const equals(char const* const s) const;


		/*
			Converts the contained string to a string.

			\return String of the contained string.
		*/
		std::string toString() const;

		void const write(UPKWriter& output) const override;
};