#include "FString.h"


FString::FString(UPKReader& input){
	int32_t length;
	input.read(length);
	if(length == 1){
		throw std::runtime_error(std::string("FString::FString(...) : Investigate 1 length string!"));
	}
	if(length > 0){
		_string = input.readString(length);
	}
}

FString::~FString(){
	if(_string){
		delete[] _string;
	}
}


uint32_t const FString::size() const{
	uint32_t size = sizeof(int32_t);
	if(_string){
		size += strlen(_string) + 1;
	}
	return size;
}


bool const FString::equals(char const* const s) const{
	if(_string){
		return strcmp(_string, s) == 0;
	}
	return s == nullptr;
}


std::string FString::toString() const{
	if(_string){
		return std::string(_string);
	}
	throw std::runtime_error("FString::toString() : Investigate null toString()!");
	return nullptr;
}

void const FString::write(UPKWriter& output) const{
	if(_string){
		output.write((int32_t) std::strlen(_string) + 1);
		output.writeString(_string);
	}
	else{
		output.write((int32_t) 0);
	}
}