#include "FGuid.h"


FGuid const FGuid::ZERO;


FGuid::FGuid(uint32_t const& a, uint32_t const& b, uint32_t const& c, uint32_t const& d){
	_guid[3] = a;
	_guid[2] = b;
	_guid[1] = c;
	_guid[0] = d;
}

FGuid::FGuid(UPKReader& input){
	input.read(_guid[3]);
	input.read(_guid[2]);
	input.read(_guid[1]);
	input.read(_guid[0]);
}


uint32_t const FGuid::size() const{
	return sizeof(_guid);
}


bool const FGuid::equals(FGuid const& g) const{
	for(uint8_t i = 0; i < 4; i++){
		if(_guid[i] != g._guid[i]){
			return false;
		}
	}
	return true;
}


void const FGuid::dump(FileWriter& output, uint8_t const& indent = 0) const{
	output.write(toString());
}

std::string FGuid::toString() const{
	return UPKReader::toHexString(_guid, sizeof(_guid), false);
}

void const FGuid::write(UPKWriter& output) const{
	output.write(_guid[3]);
	output.write(_guid[2]);
	output.write(_guid[1]);
	output.write(_guid[0]);
}