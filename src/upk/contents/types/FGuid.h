#pragma once

#include "../Object.h"


class FGuid : public Object{
	private:
		/*
			Contained globally unique identifier.
		*/
		uint32_t _guid[4];


	public:
		/*
			GUID with no unique value.
		*/
		static FGuid const ZERO;


		/*
			Creates a globally unique identifier from data.

			\param 'a' - 'd' Individual parts of the GUID.
		*/
		FGuid(uint32_t const& a = 0, uint32_t const& b = 0, uint32_t const& c = 0, uint32_t const& d = 0);

		/*
			Creates a globally unique identifier from a UPK file.

			\param 'input' Input UPK file reader.
		*/
		FGuid(UPKReader& input);


		uint32_t const size() const override;


		/*
			Checks if the GUID is equal to another GUID.

			\param 'g' GUID to check.

			\return 'true' if GUIDs are equal.
		*/
		bool const equals(FGuid const& g) const;


		void const dump(FileWriter& output, uint8_t const& indent = 0) const override;

		/*
			Converts the GUID to a string.

			\return String of the GUID.
		*/
		std::string toString() const;

		void const write(UPKWriter& output) const override;
};