#pragma once

#include "TArray.h"
#include "TPair.h"


template<class TK, class TI> class TMap : public Object{
	private:
		/*
			Map of unique key to value pairs.
		*/
		TArray<TPair<TK, TI>>* _map;


	public:
		/*
			Creates a unique key map of objects.

			\param 'input' Input UPK file reader.
		*/
		TMap(UPKReader& input){
			_map = new TArray<TPair<TK, TI>>(input);
		}

		~TMap(){
			delete _map;
		}


		/*
			Gets the number of elements in the mapping.

			\return Number of elements in the mapping.
		*/
		int32_t const elements() const{
			return _map->elements();
		}

		/*
			Gets a map entry.

			\param 'index' Index of the entry.

			\return Map entry.
		*/
		TPair<TK, TI> const* const get(int32_t const& index) const{
			return _map->get(index);
		}

		uint32_t const size() const override{
			return _map->size();
		}


		void const write(UPKWriter& output) const override{
			_map->write(output);
		}
};