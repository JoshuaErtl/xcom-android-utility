#include "UConst.h"


UConst::UConst(UPKReader& input, FPackageFileSummary const& summary, FObjectExport const& object):
	UField(input, summary, object){
	_value = new FString(input);
}

UConst::~UConst(){
	delete _value;
}


uint32_t const UConst::size() const{
	return UField::size() + _value->size();
}


void const UConst::write(UPKWriter& output) const{
	UField::write(output);
	_value->write(output);
}