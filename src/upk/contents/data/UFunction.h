#pragma once

#include "UStruct.h"


class UFunction : public UStruct{
	public:
		enum FunctionFlags: uint32_t{
			Final						= 0x00000001, // Final function
			Defined						= 0x00000002, // Function has been defined with script
			Iterator					= 0x00000004, // Iterator function
			Latent						= 0x00000008, // Latent state function
			Singular					= 0x00000020, // Cannot be reentered
			Net							= 0x00000040, // Is network-replicated
			NetReliable					= 0x00000080, // Should be sent reliably on the network
			Simulated					= 0x00000100, // Executed on client side
			Exec						= 0x00000200, // Executable from command line
			Native						= 0x00000400, // Native function
			Event						= 0x00000800, // Event function
			Static						= 0x00002000, // Static function
			NoExport_OptionalParameters	= 0x00004000, // Don't export function to C++ (TODO: Or unknown?)
			Public						= 0x00020000, // TODO: Public?
			Private						= 0x00040000, // TODO: Private?
			Protected					= 0x00080000, // TODO: Protected?
			Delegate					= 0x00100000, // TODO: Delegate function?
			Unknown1					= 0x00400000, // TODO: Unknown?
			Unknown2					= 0x00800000, // TODO: Unknown?
			NetServer					= 0x00200000, // TODO: Net server?
			NetClient					= 0x01000000 // TODO: Net client?
		};


	private:
		/*
			Indicator for properties of the function.
		*/
		int32_t _functionFlags;

		/*
			TODO: Token for native function?
		*/
		int16_t _nativeToken;

		/*
			TODO: Pre or post operator?
		*/
		int8_t _operatorPrecedence;

		/*
			TODO: Unknown?
		*/
		int16_t _repOffset;


	public:
		/*
			Creates a function data object.

			\param 'input' Input UPK file reader.
			\param 'summary' Summary of UPK file.
			\param 'object' Object that owns this data.
		*/
		UFunction(UPKReader& input, FPackageFileSummary const& summary, FObjectExport const& object);


		uint32_t const size() const override;


		/*
			Checks if the function has a flag property.

			\param 'flag' Flag to check.

			\return 'true' if the function has the flag.
		*/
		bool const hasFlag(FunctionFlags const& flag) const;


		void const write(UPKWriter& output) const override;
};