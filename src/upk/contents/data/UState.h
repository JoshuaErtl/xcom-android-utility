#pragma once

#include "UStruct.h"
#include "../types/TMap.h"


class UState : public UStruct{
	public:
		enum StateFlags: uint32_t{
			Auto		= 0x00000002, // Is in automatic, default state
			Simulated	= 0x00000004 // Executes on client side
		};


	private:
		/*
			Memory offset of 'LabelTable' inside of '_dataScript'.
		*/
		int16_t _labelTableOffset;

		/*
			TODO: Unknown?
		*/
		int32_t _probeMask;

		/*
			Indicator for properties of the state.
		*/
		int32_t _stateFlags;

		/*
			Map of state names to state objects.
		*/
		TMap<UNameIndex, UObjectReference>* _stateMap;


	public:
		/*
			Creates a state data object.

			\param 'input' Input UPK file reader.
			\param 'summary' Summary of UPK file.
			\param 'object' Object that owns this data.
		*/
		UState(UPKReader& input, FPackageFileSummary const& summary, FObjectExport const& object);

		~UState();


		uint32_t const size() const override;


		void const write(UPKWriter& output) const override;
};