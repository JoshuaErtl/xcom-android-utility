#pragma once

#include "../Object.h"


class ByteCode : public Object{
	private:
		/*
			Interal script's code.
		*/
		uint8_t* _dataScript;

		/*
			Data size of script.
		*/
		int32_t _serialSize;


	public:
		/*
			Creates a container for a script's code.

			\param 'input' Input UPK file reader.
			\param 'serialSize' Size of script.
		*/
		ByteCode(UPKReader& input, int32_t const& serialSize);

		~ByteCode();


		uint32_t const size() const override;


		void const write(UPKWriter& output) const override;
};