#include "UStruct.h"


UStruct::UStruct(UPKReader& input, FPackageFileSummary const& summary, FObjectExport const& object):
	UField(input, summary, object){
	_firstChildReference = new UObjectReference(input);
	input.read(_scriptMemorySize);
	int32_t scriptSerialSize;
	input.read(scriptSerialSize);
	_dataScript = new ByteCode(input, scriptSerialSize);
}

UStruct::~UStruct(){
	delete _dataScript;
	delete _firstChildReference;
}


uint32_t const UStruct::size() const{
	return UField::size() + sizeof(int32_t) + _dataScript->size() + _firstChildReference->size() + sizeof(_scriptMemorySize);
}


void const UStruct::write(UPKWriter& output) const{
	UField::write(output);
	_firstChildReference->write(output);
	output.write(_scriptMemorySize);
	output.write((int32_t) _dataScript->size());
	_dataScript->write(output);
}