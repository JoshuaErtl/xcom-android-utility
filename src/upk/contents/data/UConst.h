#pragma once

#include "UField.h"
#include "../types/FString.h"


class UConst : public UField{
	private:
		/*
			String value.
		*/
		FString* _value;


	public:
		/*
			Creates a constant data object.

			\param 'input' Input UPK file reader.
			\param 'summary' Summary of UPK file.
			\param 'object' Object that owns this data.
		*/
		UConst(UPKReader& input, FPackageFileSummary const& summary, FObjectExport const& object);

		~UConst();


		uint32_t const size() const override;


		void const write(UPKWriter& output) const override;
};