#pragma once

#include <vector>

#include "UDefaultProperty.h"


class UDefaultPropertyList : public Object{
	private:
		/*
			List of default properties.
		*/
		std::vector<UDefaultProperty*> _list;

		/*
			Name index of 'None' entry.
		*/
		UNameIndex* _noneIndex;


	public:
		/*
			Creates a default property list.

			\param 'input' Input UPK file reader.
			\param 'summary' Summary of UPK file.
		*/
		UDefaultPropertyList(UPKReader& input, FPackageFileSummary const& summary);

		~UDefaultPropertyList();


		uint32_t const size() const override;


		void const write(UPKWriter& output) const override;
};