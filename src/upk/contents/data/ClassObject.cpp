#include "ClassObject.h"


ClassObject::ClassObject(UPKReader& input, FPackageFileSummary const& summary, FObjectExport const& object, int32_t const& serialSize):
	UObject(object),
	_serialSize(serialSize){
	//TODO
	_data = new uint8_t[serialSize];
	for(int32_t i = 0; i < serialSize; i++){
		input.read(_data[i]);
	}
}

ClassObject::~ClassObject(){
	delete[] _data;
}


uint32_t const ClassObject::size() const{
	return _serialSize;
}


void const ClassObject::write(UPKWriter& output) const{
	for(int32_t i = 0; i < _serialSize; i++){
		output.write(_data[i]);
	}
}