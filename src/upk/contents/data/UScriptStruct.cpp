#include "UScriptStruct.h"


UScriptStruct::UScriptStruct(UPKReader& input, FPackageFileSummary const& summary, FObjectExport const& object):
	UStruct(input, summary, object){
	input.read(_structFlags);
	{
		uint32_t const structFlagsValues[] = {StructFlags::Native, StructFlags::Export, StructFlags::Long_HasComponents, StructFlags::Init_Transient, StructFlags::Atomic, StructFlags::Immutable, StructFlags::ImmutableWhenCooked, StructFlags::AtomicWhenCooked};
		checkFlagsAssigned(_structFlags, structFlagsValues, 8, "UScriptStruct::UScriptStruct(...)", "Struct");
	}
	_structDefaultProperties = new UDefaultPropertyList(input, summary);
}

UScriptStruct::~UScriptStruct(){
	delete _structDefaultProperties;
}


uint32_t const UScriptStruct::size() const{
	return UStruct::size() + _structDefaultProperties->size() + sizeof(_structFlags);
}


void const UScriptStruct::write(UPKWriter& output) const{
	UStruct::write(output);
	{
		uint32_t const structFlagsValues[] = {StructFlags::Native, StructFlags::Export, StructFlags::Long_HasComponents, StructFlags::Init_Transient, StructFlags::Atomic, StructFlags::Immutable, StructFlags::ImmutableWhenCooked, StructFlags::AtomicWhenCooked};
		checkFlagsAssigned(_structFlags, structFlagsValues, 8, "UScriptStruct::UScriptStruct(...)", "Struct");
	}
	output.write(_structFlags);
	_structDefaultProperties->write(output);
}