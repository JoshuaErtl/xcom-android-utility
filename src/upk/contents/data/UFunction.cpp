#include "UFunction.h"

#include <string>


UFunction::UFunction(UPKReader& input, FPackageFileSummary const& summary, FObjectExport const& object):
	UStruct(input, summary, object){
	input.read(_nativeToken);
	input.read(_operatorPrecedence);
	if(_operatorPrecedence != 0){
		throw std::runtime_error(std::string("UFunction::UFunction(...) : Operator precedence not 0!\n\t(Operator Precedence: ").append(std::to_string(_operatorPrecedence)).append(")"));
	}
	input.read(_functionFlags);
	{
		uint32_t const functionFlagsValues[] = {FunctionFlags::Final, FunctionFlags::Defined, FunctionFlags::Iterator, FunctionFlags::Latent, FunctionFlags::Singular, FunctionFlags::Net, FunctionFlags::NetReliable, FunctionFlags::Simulated, FunctionFlags::Exec, FunctionFlags::Native, FunctionFlags::Event, FunctionFlags::Static, FunctionFlags::NoExport_OptionalParameters, FunctionFlags::Public, FunctionFlags::Private, FunctionFlags::Protected, FunctionFlags::Delegate, FunctionFlags::Unknown1, FunctionFlags::Unknown2, FunctionFlags::NetServer, FunctionFlags::NetClient};
		checkFlagsAssigned(_functionFlags, functionFlagsValues, 21, "UFunction::UFunction(...)", "Function");
	}
	if(hasFlag(FunctionFlags::Net)){
		input.read(_repOffset);
	}
}


uint32_t const UFunction::size() const{
	uint32_t size = UStruct::size() + sizeof(_functionFlags) + sizeof(_nativeToken) + sizeof(_operatorPrecedence);
	if(hasFlag(FunctionFlags::Net)){
		size += sizeof(_repOffset);
	}
	return size;
}


bool const UFunction::hasFlag(FunctionFlags const& flag) const{
	return (_functionFlags & flag) > 0;
}


void const UFunction::write(UPKWriter& output) const{
	UStruct::write(output);
	output.write(_nativeToken);
	if(_operatorPrecedence != 0){
		throw std::runtime_error(std::string("UFunction::UFunction(...) : Operator precedence not 0!\n\t(Operator Precedence: ").append(std::to_string(_operatorPrecedence)).append(")"));
	}
	output.write(_operatorPrecedence);
	{
		uint32_t const functionFlagsValues[] = {FunctionFlags::Final, FunctionFlags::Defined, FunctionFlags::Iterator, FunctionFlags::Latent, FunctionFlags::Singular, FunctionFlags::Net, FunctionFlags::NetReliable, FunctionFlags::Simulated, FunctionFlags::Exec, FunctionFlags::Native, FunctionFlags::Event, FunctionFlags::Static, FunctionFlags::NoExport_OptionalParameters, FunctionFlags::Public, FunctionFlags::Private, FunctionFlags::Protected, FunctionFlags::Delegate, FunctionFlags::Unknown1, FunctionFlags::Unknown2, FunctionFlags::NetServer, FunctionFlags::NetClient};
		checkFlagsAssigned(_functionFlags, functionFlagsValues, 21, "UFunction::UFunction(...)", "Function");
	}
	output.write(_functionFlags);
	if(hasFlag(FunctionFlags::Net)){
		output.write(_repOffset);
	}
}