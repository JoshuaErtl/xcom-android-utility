#pragma once

#include "values/PropertyValue.h"
#include "../UNameIndex.h"


class UDefaultProperty : public Object{
	private:
		/*
			If fixed array, array element index starting at 1, otherwise 0.
		*/
		int32_t _arrayIndex;

		/*
			If 'BoolProperty' type, '0' if 'false' or '1' if 'true'.
		*/
		int8_t _boolValue;

		/*
			If 'ByteProperty', enumeration object name type of value.
		*/
		UNameIndex* _enumNameIndex = nullptr;

		/*
			Property name index.
		*/
		UNameIndex* _nameIndex;

		/*
			Default value of the property.
		*/
		PropertyValue* _propertyValue = nullptr;

		/*
			If 'StructProperty', structure name defining internal data type.
		*/
		UNameIndex* _structNameIndex = nullptr;

		/*
			Summary of UPK file.
		*/
		FPackageFileSummary const* const _summary;

		/*
			Property type name index.
		*/
		UNameIndex* _typeIndex;


	public:
		/*
			Creates a default property.

			\param 'input' Input UPK file reader.
			\param 'nameIndex' Property name index.
			\param 'summary' Summary of UPK file.
		*/
		UDefaultProperty(UPKReader& input, UNameIndex* nameIndex, FPackageFileSummary const& summary);

		~UDefaultProperty();


		uint32_t const size() const override;


		void const write(UPKWriter& output) const override;
};