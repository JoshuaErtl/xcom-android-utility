#pragma once

#include "UObject.h"


class ClassObject : public UObject{
	private:
		/*
			Internal data.
		*/
		uint8_t* _data;

		/*
			Size of external data.
		*/
		int32_t _serialSize;


	public:
		/*
			Creates a class object data object.

			\param 'input' Input UPK file reader.
			\param 'summary' Summary of UPK file.
			\param 'object' Object that owns this data.
			\param 'serialSize' Size of external data.
		*/
		ClassObject(UPKReader& input, FPackageFileSummary const& summary, FObjectExport const& object, int32_t const& serialSize);

		~ClassObject();


		uint32_t const size() const override;


		void const write(UPKWriter& output) const override;
};