#include "UDefaultProperty.h"

#include <string>

#include "values/ArrayProperty.h"
#include "values/ByteProperty.h"
#include "values/DelegateProperty.h"
#include "values/FloatProperty.h"
#include "values/IntProperty.h"
#include "values/NameProperty.h"
#include "values/ObjectProperty.h"
#include "values/RawProperty.h"
#include "values/StrProperty.h"
#include "values/StructProperty.h"


UDefaultProperty::UDefaultProperty(UPKReader& input, UNameIndex* nameIndex, FPackageFileSummary const& summary):
	_nameIndex(nameIndex),
	_summary(&summary){
	_typeIndex = new UNameIndex(input);
	int32_t propertySize;
	input.read(propertySize);
	input.read(_arrayIndex);
	std::string const type = _typeIndex->toString(summary);
	if(type == "BoolProperty"){
		input.read(_boolValue);
	}
	else if(type == "IntProperty"){
		if(propertySize != IntProperty::propertySize()){
			throw std::runtime_error(std::string("UDefaultProperty::UDefaultProperty(..., ").append(nameIndex->toString(summary)).append(", ...) : IntProperty not correct size!\n\t(Property Size: ").append(std::to_string(propertySize)).append("  Expected Size: ").append(std::to_string(IntProperty::propertySize())).append(")"));
		}
		_propertyValue = new IntProperty(input);
	}
	else if(type == "FloatProperty"){
		if(propertySize != FloatProperty::propertySize()){
			throw std::runtime_error(std::string("UDefaultProperty::UDefaultProperty(..., ").append(nameIndex->toString(summary)).append(", ...) : FloatProperty not correct size!\n\t(Property Size: ").append(std::to_string(propertySize)).append("  Expected Size: ").append(std::to_string(FloatProperty::propertySize())).append(")"));
		}
		_propertyValue = new FloatProperty(input);
	}
	else if(type == "ObjectProperty"){
		if(propertySize != ObjectProperty::propertySize()){
			throw std::runtime_error(std::string("UDefaultProperty::UDefaultProperty(..., ").append(nameIndex->toString(summary)).append(", ...) : ObjectProperty not correct size!\n\t(Property Size: ").append(std::to_string(propertySize)).append("  Expected Size: ").append(std::to_string(ObjectProperty::propertySize())).append(")"));
		}
		_propertyValue = new ObjectProperty(input);
	}
	else if(type == "NameProperty"){
		if(propertySize != NameProperty::propertySize()){
			throw std::runtime_error(std::string("UDefaultProperty::UDefaultProperty(..., ").append(nameIndex->toString(summary)).append(", ...) : NameProperty not correct size!\n\t(Property Size: ").append(std::to_string(propertySize)).append("  Expected Size: ").append(std::to_string(NameProperty::propertySize())).append(")"));
		}
		_propertyValue = new NameProperty(input);
	}
	else if(type == "ByteProperty"){
		_enumNameIndex = new UNameIndex(input);
		std::string const _enumNameString = _enumNameIndex->toString(summary);
		if(propertySize == 1 || _enumNameString == "None"){
			if(propertySize == 1 && _enumNameString == "None"){
				_propertyValue = new RawProperty(input, propertySize);
			}
			else{
				throw std::runtime_error(std::string("UDefaultProperty::UDefaultProperty(..., ").append(nameIndex->toString(summary)).append(", ...) : ").append(propertySize == 1 ? "Enum name index not equal to \"None\"!" : "Property size not equal to 1!").append("\n\t(Enum Name Index: ").append(_enumNameString).append("  Property Size: ").append(std::to_string(propertySize)).append(")"));
			}
		}
		else if(propertySize != ByteProperty::propertySize()){
			throw std::runtime_error(std::string("UDefaultProperty::UDefaultProperty(..., ").append(nameIndex->toString(summary)).append(", ...) : ByteProperty not correct size!\n\t(Property Size: ").append(std::to_string(propertySize)).append("  Expected Size: ").append(std::to_string(ByteProperty::propertySize())).append(")"));
		}
		else{
			_propertyValue = new ByteProperty(input);
		}
	}
	else if(type == "StrProperty"){
		_propertyValue = new StrProperty(input);
	}
	else if(type == "StructProperty"){
		_structNameIndex = new UNameIndex(input);
		_propertyValue = new StructProperty(input, propertySize);
	}
	else if(type == "ArrayProperty"){
		_propertyValue = new ArrayProperty(input, propertySize);
	}
	else if(type == "DelegateProperty"){
		if(propertySize != DelegateProperty::propertySize()){
			throw std::runtime_error(std::string("UDefaultProperty::UDefaultProperty(..., ").append(nameIndex->toString(summary)).append(", ...) : DelegateProperty not correct size!\n\t(Property Size: ").append(std::to_string(propertySize)).append("  Expected Size: ").append(std::to_string(DelegateProperty::propertySize())).append(")"));
		}
		_propertyValue = new DelegateProperty(input);
	}
	else{
		throw std::runtime_error(std::string("UDefaultProperty::UDefaultProperty(..., ").append(nameIndex->toString(summary)).append(", ...) : Unhandled type!\n\t(Type: ").append(type).append(")"));
	}
	if((propertySize == 0) != (_propertyValue == nullptr)){
		throw std::runtime_error(std::string("UDefaultProperty::UDefaultProperty(..., ").append(nameIndex->toString(summary)).append(", ...) : ").append(_propertyValue ? "Property size incorrectly 0!" : "Property size expected missing property!").append("\n\t(Property Size: ").append(std::to_string(propertySize)).append(")"));
	}
	if(_propertyValue){
		uint32_t const propertyValueSize = _propertyValue->size();
		if(propertySize != propertyValueSize){
			throw std::runtime_error(std::string("UDefaultProperty::UDefaultProperty(..., ").append(nameIndex->toString(summary)).append(", ...) : Read property size does not match property value size!\n\t(Read Property Size: ").append(std::to_string(propertySize)).append("  Property Value Size: ").append(std::to_string(propertyValueSize)).append(")"));
		}
	}
}

UDefaultProperty::~UDefaultProperty(){
	if(_enumNameIndex){
		delete _enumNameIndex;
	}
	delete _nameIndex;
	if(_propertyValue){
		delete _propertyValue;
	}
	if(_structNameIndex){
		delete _structNameIndex;
	}
	delete _typeIndex;
}


uint32_t const UDefaultProperty::size() const{
	uint32_t size = sizeof(int32_t) + sizeof(_arrayIndex) + _nameIndex->size() + _typeIndex->size();
	std::string const type = _typeIndex->toString(*_summary);
	if(type == "BoolProperty"){
		size += sizeof(_boolValue);
	}
	else{
		if(type == "ByteProperty"){
			size += _enumNameIndex->size();
		}
		else if(type == "StructProperty"){
			size += _structNameIndex->size();
		}
		size += _propertyValue->size();
	}
	return size;
}


void const UDefaultProperty::write(UPKWriter& output) const{
	_nameIndex->write(output);
	_typeIndex->write(output);
	output.write((int32_t) (_propertyValue ? _propertyValue->size() : 0));
	std::string const type = _typeIndex->toString(*_summary);
	output.write(_arrayIndex);
	if(type == "BoolProperty"){
		output.write(_boolValue);
	}
	else{
		if(type == "ByteProperty"){
			_enumNameIndex->write(output);
		}
		else if(type == "StructProperty"){
			_structNameIndex->write(output);
		}
		_propertyValue->write(output);
	}
}