#include "UEnum.h"


UEnum::UEnum(UPKReader& input, FPackageFileSummary const& summary, FObjectExport const& object):
	UField(input, summary, object){
	_names = new TArray<UNameIndex>(input);
}

UEnum::~UEnum(){
	delete _names;
}


uint32_t const UEnum::size() const{
	return UField::size() + _names->size();
}


void const UEnum::dump(FileWriter& output, uint8_t const& indent = 0) const{
	UField::dump(output, indent);
	output.indent(indent).write("UEnum:").end();
	_names->dump(output, indent + 1);
}

void const UEnum::write(UPKWriter& output) const{
	UField::write(output);
	_names->write(output);
}