#pragma once

#include "UObject.h"
#include "../UObjectReference.h"


class UField : public UObject{
	private:
		/*
			Next field.
		*/
		UObjectReference* _nextReference;

		/*
			Super field reference.
		*/
		UObjectReference* _parentReference = nullptr;


	public:
		/*
			Creates a field data object.

			\param 'input' Input UPK file reader.
			\param 'summary' Summary of UPK file.
			\param 'object' Object that owns this data.
		*/
		UField(UPKReader& input, FPackageFileSummary const& summary, FObjectExport const& object);

		~UField();


		uint32_t const size() const override;


		void const write(UPKWriter& output) const override;
};