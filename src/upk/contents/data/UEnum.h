#pragma once

#include "UField.h"
#include "../types/TArray.h"


class UEnum : public UField{
	private:
		/*
			Contained enumerations.
		*/
		TArray<UNameIndex>* _names;


	public:
		/*
			Creates an enumeration data object.

			\param 'input' Input UPK file reader.
			\param 'summary' Summary of UPK file.
			\param 'object' Object that owns this data.
		*/
		UEnum(UPKReader& input, FPackageFileSummary const& summary, FObjectExport const& object);

		~UEnum();


		uint32_t const size() const override;


		void const dump(FileWriter& output, uint8_t const& indent = 0) const override;

		void const write(UPKWriter& output) const override;
};