#pragma once

#include "UState.h"


class UClass : public UState{
	public:
		enum ClassFlags: uint32_t{
			Abstract			= 0x00000001, // Class is abstract and can't be instantiated
			Compiled			= 0x00000002, // Script has been compiled successfully
			Config				= 0x00000004, // Load object configuration at construction time
			Transient			= 0x00000008, // Can't be saved, zero at save
			Parsed				= 0x00000010, // Successfully parsed
			Localized			= 0x00000020, // Class contains localized text
			RuntimeStatic		= 0x00000080, // Objects of this class are static during usage
			NoExport			= 0x00000100, // Don't export to C++ header
			Placeable			= 0x00000200, // TODO: Placeable?
			PerObjectConfig		= 0x00000400, // Handle object configuration on a per-object basis rather than per-class
			NativeReplication	= 0x00000800, // Replication handled in C++
			EditInlineNew		= 0x00001000, // TODO: Edit inline new?
			CollapseCategories	= 0x00002000, // TODO: Collapse categories?
			ExportStructs		= 0x00004000, // TODO: Export structs?
			Instanced			= 0x00200000, // TODO: Instanced?
			CacheExempt_Hidden	= 0x00800000, // TODO: Cache exempt or hidden?
			HideDropDown2		= 0x02000000, // TODO: Hide drop down?
			Unknown1			= 0x08000000, // TODO: Unknown?
			Unknown2			= 0x80000000 // TODO: Unknown?
		};


	private:
		/*
			Indicator for properties of the class.
		*/
		int32_t _classFlags;

		/*
			Components of the class.
		*/
		TMap<UNameIndex, UObjectReference>* _componentsMap;

		/*
			TODO: Unknown?
		*/
		UNameIndex* _configNameIndex;

		/*
			Object with default properties.
		*/
		UObjectReference* _defaultReference;

		/*
			TODO: Unknown?
		*/
		UNameIndex* _dllBindName;

		/*
			Interfaces of the class.
		*/
		TMap<UObjectReference, UObjectReference>* _interfaces;

		/*
			TODO: Unknown?
		*/
		UObjectReference* _withinReference;


	public:
		/*
			Creates a class data object.

			\param 'input' Input UPK file reader.
			\param 'summary' Summary of UPK file.
			\param 'object' Object that owns this data.
		*/
		UClass(UPKReader& input, FPackageFileSummary const& summary, FObjectExport const& object);

		~UClass();


		uint32_t const size() const override;


		void const write(UPKWriter& output) const override;
};