#pragma once

#include "UStruct.h"


class UScriptStruct : public UStruct{
	public:
		enum StructFlags: uint32_t{
			Native				= 0x00000001, // TODO: Native?
			Export				= 0x00000002, // TODO: Export?
			Long_HasComponents	= 0x00000004, // TODO: Long or has components?
			Init_Transient		= 0x00000008, // TODO: Init or transient?
			Atomic				= 0x00000010, // TODO: Atomic?
			Immutable			= 0x00000020, // TODO: Immutable?
			ImmutableWhenCooked	= 0x00000080, // TODO: Immutable when cooked?
			AtomicWhenCooked	= 0x00000100 // TODO: Atomic when cooked?
		};


	private:
		/*
			List of default values for structure's variables.
		*/
		UDefaultPropertyList* _structDefaultProperties;

		/*
			Indicator for properties of the structure.
		*/
		int32_t _structFlags;


	public:
		/*
			Creates a script structure data object.

			\param 'input' Input UPK file reader.
			\param 'summary' Summary of UPK file.
			\param 'object' Object that owns this data.
		*/
		UScriptStruct(UPKReader& input, FPackageFileSummary const& summary, FObjectExport const& object);

		~UScriptStruct();


		uint32_t const size() const override;


		void const write(UPKWriter& output) const override;
};