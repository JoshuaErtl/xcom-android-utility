#include "UField.h"

#include "../summary/FObjectExport.h"


UField::UField(UPKReader& input, FPackageFileSummary const& summary, FObjectExport const& object):
	UObject(input, summary, object){
	_nextReference = new UObjectReference(input);
	if(isClass(object.typeName(), "Core.Struct")){
		_parentReference = new UObjectReference(input);
	}
}

UField::~UField(){
	delete _nextReference;
	if(_parentReference){
		delete _parentReference;
	}
}


uint32_t const UField::size() const{
	uint32_t size = UObject::size() + _nextReference->size();
	if(isClass(_object->typeName(), "Core.Struct")){
		size += _parentReference->size();
	}
	return size;
}


void const UField::write(UPKWriter& output) const{
	UObject::write(output);
	_nextReference->write(output);
	if(isClass(_object->typeName(), "Core.Struct")){
		_parentReference->write(output);
	}
}