#pragma once

#include "UProperty.h"


class UStructProperty : public UProperty{
	private:
		/*
			Structure type.
		*/
		UObjectReference* _structObjectReference;


	public:
		/*
			Creates a structure property data object.

			\param 'input' Input UPK file reader.
			\param 'summary' Summary of UPK file.
			\param 'object' Object that owns this data.
		*/
		UStructProperty(UPKReader& input, FPackageFileSummary const& summary, FObjectExport const& object);

		~UStructProperty();


		uint32_t const size() const override;


		void const write(UPKWriter& output) const override;
};