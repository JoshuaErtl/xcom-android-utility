#pragma once

#include "UProperty.h"


class UByteProperty : public UProperty{
	private:
		/*
			Enumeration type.
		*/
		UObjectReference* _enumObjectReference;


	public:
		/*
			Creates a byte property data object.

			\param 'input' Input UPK file reader.
			\param 'summary' Summary of UPK file.
			\param 'object' Object that owns this data.
		*/
		UByteProperty(UPKReader& input, FPackageFileSummary const& summary, FObjectExport const& object);

		~UByteProperty();


		uint32_t const size() const override;


		void const write(UPKWriter& output) const override;
};