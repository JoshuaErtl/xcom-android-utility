#pragma once

#include "UProperty.h"


class UInterfaceProperty : public UProperty{
	private:
		/*
			Interface object reference.
		*/
		UObjectReference* _interfaceObjectReference;


	public:
		/*
			Creates an interface property data object.

			\param 'input' Input UPK file reader.
			\param 'summary' Summary of UPK file.
			\param 'object' Object that owns this data.
		*/
		UInterfaceProperty(UPKReader& input, FPackageFileSummary const& summary, FObjectExport const& object);

		~UInterfaceProperty();


		uint32_t const size() const override;


		void const write(UPKWriter& output) const override;
};