#pragma once

#include "UProperty.h"


class UStrProperty : public UProperty{
	public:
		/*
			Creates a string property data object.

			\param 'input' Input UPK file reader.
			\param 'summary' Summary of UPK file.
			\param 'object' Object that owns this data.
		*/
		UStrProperty(UPKReader& input, FPackageFileSummary const& summary, FObjectExport const& object);
};