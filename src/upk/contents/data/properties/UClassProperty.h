#pragma once

#include "UObjectProperty.h"


class UClassProperty : public UObjectProperty{
	private:
		/*
			Class object reference.
		*/
		UObjectReference* _classObjectReference;


	public:
		/*
			Creates a class property data object.

			\param 'input' Input UPK file reader.
			\param 'summary' Summary of UPK file.
			\param 'object' Object that owns this data.
		*/
		UClassProperty(UPKReader& input, FPackageFileSummary const& summary, FObjectExport const& object);

		~UClassProperty();


		uint32_t const size() const override;


		void const write(UPKWriter& output) const override;
};