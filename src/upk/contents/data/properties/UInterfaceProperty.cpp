#include "UInterfaceProperty.h"


UInterfaceProperty::UInterfaceProperty(UPKReader& input, FPackageFileSummary const& summary, FObjectExport const& object):
	UProperty(input, summary, object){
	_interfaceObjectReference = new UObjectReference(input);
}

UInterfaceProperty::~UInterfaceProperty(){
	delete _interfaceObjectReference;
}


uint32_t const UInterfaceProperty::size() const{
	return UProperty::size() + _interfaceObjectReference->size();
}


void const UInterfaceProperty::write(UPKWriter& output) const{
	UProperty::write(output);
	_interfaceObjectReference->write(output);
}