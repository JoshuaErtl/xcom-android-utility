#include "UDelegateProperty.h"


UDelegateProperty::UDelegateProperty(UPKReader& input, FPackageFileSummary const& summary, FObjectExport const& object):
	UProperty(input, summary, object){
	_functionObjectReference = new UObjectReference(input);
	_delegateObjectReference = new UObjectReference(input);
}

UDelegateProperty::~UDelegateProperty(){
	delete _delegateObjectReference;
	delete _functionObjectReference;
}


uint32_t const UDelegateProperty::size() const{
	return UProperty::size() + _delegateObjectReference->size() + _functionObjectReference->size();
}


void const UDelegateProperty::write(UPKWriter& output) const{
	UProperty::write(output);
	_functionObjectReference->write(output);
	_delegateObjectReference->write(output);
}