#pragma once

#include "UProperty.h"


class UDelegateProperty : public UProperty{
	private:
		/*
			Delegate object reference.
		*/
		UObjectReference* _delegateObjectReference;

		/*
			Function object reference.
		*/
		UObjectReference* _functionObjectReference;


	public:
		/*
			Creates a delegate property data object.

			\param 'input' Input UPK file reader.
			\param 'summary' Summary of UPK file.
			\param 'object' Object that owns this data.
		*/
		UDelegateProperty(UPKReader& input, FPackageFileSummary const& summary, FObjectExport const& object);

		~UDelegateProperty();


		uint32_t const size() const override;


		void const write(UPKWriter& output) const override;
};