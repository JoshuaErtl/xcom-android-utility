#pragma once

#include "UProperty.h"


class UArrayProperty : public UProperty{
	private:
		/*
			Inner object type.
		*/
		UObjectReference* _innerObjectReference;


	public:
		/*
			Creates an array property data object.

			\param 'input' Input UPK file reader.
			\param 'summary' Summary of UPK file.
			\param 'object' Object that owns this data.
		*/
		UArrayProperty(UPKReader& input, FPackageFileSummary const& summary, FObjectExport const& object);

		~UArrayProperty();


		uint32_t const size() const override;


		void const write(UPKWriter& output) const override;
};