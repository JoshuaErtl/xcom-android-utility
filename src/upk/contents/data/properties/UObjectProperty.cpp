#include "UObjectProperty.h"


UObjectProperty::UObjectProperty(UPKReader& input, FPackageFileSummary const& summary, FObjectExport const& object):
	UProperty(input, summary, object){
	_otherObjectReference = new UObjectReference(input);
}

UObjectProperty::~UObjectProperty(){
	delete _otherObjectReference;
}


uint32_t const UObjectProperty::size() const{
	return UProperty::size() + _otherObjectReference->size();
}


void const UObjectProperty::write(UPKWriter& output) const{
	UProperty::write(output);
	_otherObjectReference->write(output);
}