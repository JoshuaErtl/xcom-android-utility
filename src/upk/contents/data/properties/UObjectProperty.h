#pragma once

#include "UProperty.h"


class UObjectProperty : public UProperty{
	private:
		/*
			Other object reference.
		*/
		UObjectReference* _otherObjectReference;


	public:
		/*
			Creates an object property data object.

			\param 'input' Input UPK file reader.
			\param 'summary' Summary of UPK file.
			\param 'object' Object that owns this data.
		*/
		UObjectProperty(UPKReader& input, FPackageFileSummary const& summary, FObjectExport const& object);

		~UObjectProperty();


		uint32_t const size() const override;


		void const write(UPKWriter& output) const override;
};