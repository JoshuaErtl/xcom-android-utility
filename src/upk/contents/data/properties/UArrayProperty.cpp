#include "UArrayProperty.h"


UArrayProperty::UArrayProperty(UPKReader& input, FPackageFileSummary const& summary, FObjectExport const& object):
	UProperty(input, summary, object){
	_innerObjectReference = new UObjectReference(input);
}

UArrayProperty::~UArrayProperty(){
	delete _innerObjectReference;
}


uint32_t const UArrayProperty::size() const{
	return UProperty::size() + _innerObjectReference->size();
}


void const UArrayProperty::write(UPKWriter& output) const{
	UProperty::write(output);
	_innerObjectReference->write(output);
}