#pragma once

#include "UProperty.h"


class UBoolProperty : public UProperty{
	public:
		/*
			Creates a boolean property data object.

			\param 'input' Input UPK file reader.
			\param 'summary' Summary of UPK file.
			\param 'object' Object that owns this data.
		*/
		UBoolProperty(UPKReader& input, FPackageFileSummary const& summary, FObjectExport const& object);
};