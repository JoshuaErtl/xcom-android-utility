#include "UClassProperty.h"


UClassProperty::UClassProperty(UPKReader& input, FPackageFileSummary const& summary, FObjectExport const& object):
	UObjectProperty(input, summary, object){
	_classObjectReference = new UObjectReference(input);
}

UClassProperty::~UClassProperty(){
	delete _classObjectReference;
}


uint32_t const UClassProperty::size() const{
	return UObjectProperty::size() + _classObjectReference->size();
}


void const UClassProperty::write(UPKWriter& output) const{
	UObjectProperty::write(output);
	_classObjectReference->write(output);
}