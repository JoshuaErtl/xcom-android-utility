#pragma once

#include "UProperty.h"


class UFloatProperty : public UProperty{
	public:
		/*
			Creates a float property data object.

			\param 'input' Input UPK file reader.
			\param 'summary' Summary of UPK file.
			\param 'object' Object that owns this data.
		*/
		UFloatProperty(UPKReader& input, FPackageFileSummary const& summary, FObjectExport const& object);
};