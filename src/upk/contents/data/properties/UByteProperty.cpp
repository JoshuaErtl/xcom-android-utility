#include "UByteProperty.h"


UByteProperty::UByteProperty(UPKReader& input, FPackageFileSummary const& summary, FObjectExport const& object):
	UProperty(input, summary, object){
	_enumObjectReference = new UObjectReference(input);
}

UByteProperty::~UByteProperty(){
	delete _enumObjectReference;
}


uint32_t const UByteProperty::size() const{
	return UProperty::size() + _enumObjectReference->size();
}


void const UByteProperty::write(UPKWriter& output) const{
	UProperty::write(output);
	_enumObjectReference->write(output);
}