#pragma once

#include "UProperty.h"


class UNameProperty : public UProperty{
	public:
		/*
			Creates a name property data object.

			\param 'input' Input UPK file reader.
			\param 'summary' Summary of UPK file.
			\param 'object' Object that owns this data.
		*/
		UNameProperty(UPKReader& input, FPackageFileSummary const& summary, FObjectExport const& object);
};