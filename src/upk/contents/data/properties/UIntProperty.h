#pragma once

#include "UProperty.h"


class UIntProperty : public UProperty{
	public:
		/*
			Creates an integer property data object.

			\param 'input' Input UPK file reader.
			\param 'summary' Summary of UPK file.
			\param 'object' Object that owns this data.
		*/
		UIntProperty(UPKReader& input, FPackageFileSummary const& summary, FObjectExport const& object);
};