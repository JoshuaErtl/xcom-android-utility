#include "UProperty.h"


UProperty::UProperty(UPKReader& input, FPackageFileSummary const& summary, FObjectExport const& object):
	UField(input, summary, object){
	input.read(_propertyFlagsH);
	{
		uint32_t const propertyFlagsHValues[] = {PropertyFlagsH::RepNotify, PropertyFlagsH::Interp, PropertyFlagsH::NonTransactional, PropertyFlagsH::EditorOnly, PropertyFlagsH::NotForConsole, PropertyFlagsH::PrivateWrite};
		checkFlagsAssigned(_propertyFlagsH, propertyFlagsHValues, 6, "UProperty::UProperty(...)", "Property high");
	}
	input.read(_propertyFlagsL);
	{
		uint32_t const propertyFlagsLValues[] = {PropertyFlagsL::Editable, PropertyFlagsL::Const, PropertyFlagsL::Input, PropertyFlagsL::ExportObject, PropertyFlagsL::OptionalParm, PropertyFlagsL::Net, PropertyFlagsL::EditConstArray_EditFixedSize_ConstRef, PropertyFlagsL::Parm, PropertyFlagsL::OutParm, PropertyFlagsL::ReturnParm, PropertyFlagsL::CoerceParm, PropertyFlagsL::Native, PropertyFlagsL::Transient, PropertyFlagsL::Config, PropertyFlagsL::Localized, PropertyFlagsL::EditConst, PropertyFlagsL::GlobalConfig, PropertyFlagsL::Component, PropertyFlagsL::OnDemand, PropertyFlagsL::New_DuplicateTransient, PropertyFlagsL::NeedCtorLink, PropertyFlagsL::NoExport, PropertyFlagsL::Cache_NoImport, PropertyFlagsL::EditorData_NoClear, PropertyFlagsL::EditInline, PropertyFlagsL::Deprecated, PropertyFlagsL::EditInlineNotify_DataBinding, PropertyFlagsL::SerializeText_Automated};
		checkFlagsAssigned(_propertyFlagsL, propertyFlagsLValues, 28, "UProperty::UProperty(...)", "Property low");
	}
	_arrayEnumReference = new UObjectReference(input);
	if(hasFlag(PropertyFlagsL::Net)){
		input.read(_repOffset);
	}
}

UProperty::~UProperty(){
	delete _arrayEnumReference;
}


uint32_t const UProperty::size() const{
	uint32_t size = UField::size() + _arrayEnumReference->size() + sizeof(_propertyFlagsH) + sizeof(_propertyFlagsL);
	if(hasFlag(PropertyFlagsL::Net)){
		size += sizeof(_repOffset);
	}
	return size;
}


bool const UProperty::hasFlag(PropertyFlagsH const& flag) const{
	return (_propertyFlagsH & flag) > 0;
}
bool const UProperty::hasFlag(PropertyFlagsL const& flag) const{
	return (_propertyFlagsL & flag) > 0;
}


void const UProperty::write(UPKWriter& output) const{
	UField::write(output);
	{
		uint32_t const propertyFlagsHValues[] = {PropertyFlagsH::RepNotify, PropertyFlagsH::Interp, PropertyFlagsH::NonTransactional, PropertyFlagsH::EditorOnly, PropertyFlagsH::NotForConsole, PropertyFlagsH::PrivateWrite};
		checkFlagsAssigned(_propertyFlagsH, propertyFlagsHValues, 6, "UProperty::write(...)", "Property high");
	}
	output.write(_propertyFlagsH);
	{
		uint32_t const propertyFlagsLValues[] = {PropertyFlagsL::Editable, PropertyFlagsL::Const, PropertyFlagsL::Input, PropertyFlagsL::ExportObject, PropertyFlagsL::OptionalParm, PropertyFlagsL::Net, PropertyFlagsL::EditConstArray_EditFixedSize_ConstRef, PropertyFlagsL::Parm, PropertyFlagsL::OutParm, PropertyFlagsL::ReturnParm, PropertyFlagsL::CoerceParm, PropertyFlagsL::Native, PropertyFlagsL::Transient, PropertyFlagsL::Config, PropertyFlagsL::Localized, PropertyFlagsL::EditConst, PropertyFlagsL::GlobalConfig, PropertyFlagsL::Component, PropertyFlagsL::OnDemand, PropertyFlagsL::New_DuplicateTransient, PropertyFlagsL::NeedCtorLink, PropertyFlagsL::NoExport, PropertyFlagsL::Cache_NoImport, PropertyFlagsL::EditorData_NoClear, PropertyFlagsL::EditInline, PropertyFlagsL::Deprecated, PropertyFlagsL::EditInlineNotify_DataBinding, PropertyFlagsL::SerializeText_Automated};
		checkFlagsAssigned(_propertyFlagsL, propertyFlagsLValues, 28, "UProperty::write(...)", "Property low");
	}
	output.write(_propertyFlagsL);
	_arrayEnumReference->write(output);
	if(hasFlag(PropertyFlagsL::Net)){
		output.write(_repOffset);
	}
}