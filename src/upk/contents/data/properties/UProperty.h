#pragma once

#include "../UField.h"


class UProperty : public UField{
	public:
		enum PropertyFlagsH: uint32_t{
			RepNotify			= 0x00000001, // TODO: Replication notify?
			Interp				= 0x00000002, // TODO: Interpreted?
			NonTransactional	= 0x00000004, // TODO: Non-transactional?
			EditorOnly			= 0x00000008, // TODO: Editor only?
			NotForConsole		= 0x00000010, // TODO: Not for console?
			PrivateWrite		= 0x00000040 // TODO: Private write?
		};
		enum PropertyFlagsL: uint32_t{
			Editable								= 0x00000001, // Settable in editor
			Const									= 0x00000002, // Constant default class value
			Input									= 0x00000004, // Variable is writable by input system
			ExportObject							= 0x00000008, // Object can be exported with actor
			OptionalParm							= 0x00000010, // Optional parameter
			Net										= 0x00000020, // Property is relevant to net replication
			EditConstArray_EditFixedSize_ConstRef	= 0x00000040, // (TODO: Unknown?) (TODO: Or unknown?) Or reference to a constant object
			Parm									= 0x00000080, // Function call parameter
			OutParm									= 0x00000100, // Value is copied out after function call
			ReturnParm								= 0x00000400, // Return value
			CoerceParm								= 0x00000800, // Coerce arguments into this parameter
			Native									= 0x00001000, // C++ is responsible for serializing
			Transient								= 0x00002000, // Shouldn't be saved, zero at load
			Config									= 0x00004000, // Loaded/saved as permanent profile
			Localized								= 0x00008000, // Loaded as localizable text
			EditConst								= 0x00020000, // Uneditable in the editor
			GlobalConfig							= 0x00040000, // Load config from base class, not subclass
			Component								= 0x00080000, // TODO: Component?
			OnDemand								= 0x00100000, // Loaded on demand only
			New_DuplicateTransient					= 0x00200000, // Automatically create inner object (TODO: Or unknown?)
			NeedCtorLink							= 0x00400000, // Fields need construction/destruction
			NoExport								= 0x00800000, // TODO: No export?
			Cache_NoImport							= 0x01000000, // TODO: Cache or no import?
			EditorData_NoClear						= 0x02000000, // TODO: Editor data or no clear?
			EditInline								= 0x04000000, // TODO: Edit inline?
			Deprecated								= 0x20000000, // TODO: Deprecated
			EditInlineNotify_DataBinding			= 0x40000000, // TODO: Edit inline or data binding?
			SerializeText_Automated					= 0x80000000 // TODO: Serialize text or automated?
		};


	private:
		/*
			TODO: Array enumeration reference?
		*/
		UObjectReference* _arrayEnumReference;

		/*
			Indicator for properties of the property.
		*/
		int32_t _propertyFlagsH;
		/*
			Indicator for properties of the property.
		*/
		int32_t _propertyFlagsL;

		/*
			// TODO: Unknown?
		*/
		int16_t _repOffset;


	public:
		/*
			Creates a property data object.

			\param 'input' Input UPK file reader.
			\param 'summary' Summary of UPK file.
			\param 'object' Object that owns this data.
		*/
		UProperty(UPKReader& input, FPackageFileSummary const& summary, FObjectExport const& object);

		~UProperty();


		uint32_t const size() const override;


		/*
			Checks if the property has a high flag property.

			\param 'flag' High flag to check.

			\return 'true' if the property has the flag.
		*/
		bool const hasFlag(PropertyFlagsH const& flag) const;
		/*
			Checks if the property has a low flag property.

			\param 'flag' Low flag to check.

			\return 'true' if the object has the flag.
		*/
		bool const hasFlag(PropertyFlagsL const& flag) const;


		void const write(UPKWriter& output) const override;
};