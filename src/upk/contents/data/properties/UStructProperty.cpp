#include "UStructProperty.h"


UStructProperty::UStructProperty(UPKReader& input, FPackageFileSummary const& summary, FObjectExport const& object):
	UProperty(input, summary, object){
	_structObjectReference = new UObjectReference(input);
}

UStructProperty::~UStructProperty(){
	delete _structObjectReference;
}


uint32_t const UStructProperty::size() const{
	return UProperty::size() + _structObjectReference->size();
}


void const UStructProperty::write(UPKWriter& output) const{
	UProperty::write(output);
	_structObjectReference->write(output);
}