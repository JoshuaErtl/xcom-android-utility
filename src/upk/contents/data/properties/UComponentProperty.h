#pragma once

#include "UObjectProperty.h"


class UComponentProperty : public UObjectProperty{
	public:
		/*
			Creates a component property data object.

			\param 'input' Input UPK file reader.
			\param 'summary' Summary of UPK file.
			\param 'object' Object that owns this data.
		*/
		UComponentProperty(UPKReader& input, FPackageFileSummary const& summary, FObjectExport const& object);
};