#pragma once

#include "ByteCode.h"
#include "UField.h"


class UStruct : public UField{
	private:
		/*
			Container for script's code.
		*/
		ByteCode* _dataScript;

		/*
			First child of the structure.
		*/
		UObjectReference* _firstChildReference;

		/*
			Memory size of script.
		*/
		int32_t _scriptMemorySize;


	public:
		/*
			Creates a structure data object.

			\param 'input' Input UPK file reader.
			\param 'summary' Summary of UPK file.
			\param 'object' Object that owns this data.
		*/
		UStruct(UPKReader& input, FPackageFileSummary const& summary, FObjectExport const& object);

		~UStruct();


		uint32_t const size() const override;


		void const write(UPKWriter& output) const override;
};