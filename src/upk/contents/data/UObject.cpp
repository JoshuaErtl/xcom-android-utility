#include "UObject.h"

#include "../summary/FObjectExport.h"


std::map<std::string const, std::string const> const UObject::HIERARCHY = {
	{"Core.Field", "Core.Object"},
		{"Core.Const", "Core.Field"},
		{"Core.Enum", "Core.Field"},
		{"Core.Property", "Core.Field"},
			{"Core.ByteProperty", "Core.Property"},
			{"Core.IntProperty", "Core.Property"},
			{"Core.BoolProperty", "Core.Property"},
			{"Core.FloatProperty", "Core.Property"},
			{"Core.ObjectProperty", "Core.Property"},
				{"Core.ClassProperty", "Core.ObjectProperty"},
				{"Core.ComponentProperty", "Core.ObjectProperty"},
			{"Core.NameProperty", "Core.Property"},
			{"Core.StructProperty", "Core.Property"},
			{"Core.StrProperty", "Core.Property"},
			{"Core.FixedArrayProperty", "Core.Property"},
			{"Core.ArrayProperty", "Core.Property"},
			{"Core.MapProperty", "Core.Property"},
			{"Core.DelegateProperty", "Core.Property"},
			{"Core.InterfaceProperty", "Core.Property"},
		{"Core.Struct", "Core.Field"},
			{"Core.ScriptStruct", "Core.Struct"},
			{"Core.Function", "Core.Struct"},
			{"Core.State", "Core.Struct"},
				{"Core.Class", "Core.State"},
	{"Core.TextBuffer", "Core.Object"},
	{"Core.Component", "Core.Object"}
};


UObject::UObject(FObjectExport const& object):
	_object(&object){}
UObject::UObject(UPKReader& input, FPackageFileSummary const& summary, FObjectExport const& object):
	_object(&object){
	input.read(_netIndex);
	if(object.hasFlag(FObjectExport::ObjectFlagsL::HasStack)){
		_unknown = new uint8_t[UNKNOWN_SIZE];
		for(int8_t i = 0; i < UNKNOWN_SIZE; i++){
			input.read(_unknown[i]);
		}
		throw std::runtime_error(std::string("UObject::UObject(...) : Analyze unknown data!"));
	}
	if(object.typeName() != "Core.Class"){
		_defaultProperties = new UDefaultPropertyList(input, summary);
	}
}

UObject::~UObject(){
	if(_defaultProperties){
		delete _defaultProperties;
	}
	if(_unknown){
		delete[] _unknown;
	}
}


uint32_t const UObject::size() const{
	uint32_t size = sizeof(_netIndex);
	if(_object->typeName() != "Core.Class"){
		size += _defaultProperties->size();
	}
	if(_object->hasFlag(FObjectExport::ObjectFlagsL::HasStack)){
		size += UNKNOWN_SIZE;
	}
	return size;
}


void const UObject::write(UPKWriter& output) const{
	output.write(_netIndex);
	if(_object->hasFlag(FObjectExport::ObjectFlagsL::HasStack)){
		for(int8_t i = 0; i < UNKNOWN_SIZE; i++){
			output.write(_unknown[i]);
		}
	}
	if(_object->typeName() != "Core.Class"){
		_defaultProperties->write(output);
	}
}


bool const UObject::isClass(std::string type, std::string const& checkClass){
	if(type == checkClass){
		return true;
	}
	std::map<std::string const, std::string const>::const_iterator found;
	while((found = HIERARCHY.find(type)) != HIERARCHY.end()){
		if((type = found->second) == checkClass){
			return true;
		}
	}
	return false;
}