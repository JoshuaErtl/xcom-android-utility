#include "ByteCode.h"


ByteCode::ByteCode(UPKReader& input, int32_t const& serialSize):
	_serialSize(serialSize){
	_dataScript = new uint8_t[serialSize];
	for(int32_t i = 0; i < serialSize; i++){
		input.read(_dataScript[i]);
	}
}

ByteCode::~ByteCode(){
	delete[] _dataScript;
}


uint32_t const ByteCode::size() const{
	return _serialSize;
}


void const ByteCode::write(UPKWriter& output) const{
	for(int32_t i = 0; i < _serialSize; i++){
		output.write(_dataScript[i]);
	}
}