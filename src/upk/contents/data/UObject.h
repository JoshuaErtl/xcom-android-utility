#pragma once

#include <map>

#include "UDefaultPropertyList.h"

class FObjectExport;


class UObject : public Object{
	private:
		/*
			Hierarchy of object types, where key is type and value is parent.
		*/
		static std::map<std::string const, std::string const> const HIERARCHY;

		/*
			Size of '_unknown' array.
		*/
		static uint8_t const UNKNOWN_SIZE = 22;


		/*
			List of default values for object's variables.
		*/
		UDefaultPropertyList* _defaultProperties = nullptr;

		/*
			Link to previous object for net replication.
		*/
		int32_t _netIndex;

		/*
			//TODO: Unknown?
		*/
		uint8_t* _unknown = nullptr;


	protected:
		/*
			Object that owns this data.
		*/
		FObjectExport const* const _object;


		/*
			Checks if a type is a class or parent of the class.

			\param 'type' Type to check.
			\param 'checkClass' Class to find.

			\return 'true' if type is a class or parent of the class.
		*/
		static bool const isClass(std::string type, std::string const& checkClass);


	public:
		/*
			Creates a data object with no unique data.  Should only be used if
			class needs this as a base.

			\param 'object' Object that owns this data.
		*/
		UObject(FObjectExport const& object);
		/*
			Creates a data object.

			\param 'input' Input UPK file reader.
			\param 'summary' Summary of UPK file.
			\param 'object' Object that owns this data.
		*/
		UObject(UPKReader& input, FPackageFileSummary const& summary, FObjectExport const& object);

		virtual ~UObject();


		uint32_t const size() const override;


		void const write(UPKWriter& output) const override;
};