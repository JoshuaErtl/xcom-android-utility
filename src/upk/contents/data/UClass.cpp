#include "UClass.h"


UClass::UClass(UPKReader& input, FPackageFileSummary const& summary, FObjectExport const& object):
	UState(input, summary, object){
	input.read(_classFlags);
	{
		uint32_t const classFlagsValues[] = {ClassFlags::Abstract, ClassFlags::Compiled, ClassFlags::Config, ClassFlags::Transient, ClassFlags::Parsed, ClassFlags::Localized, ClassFlags::RuntimeStatic, ClassFlags::NoExport, ClassFlags::Placeable, ClassFlags::PerObjectConfig, ClassFlags::NativeReplication, ClassFlags::EditInlineNew, ClassFlags::CollapseCategories, ClassFlags::ExportStructs, ClassFlags::Instanced, ClassFlags::CacheExempt_Hidden, ClassFlags::HideDropDown2, ClassFlags::Unknown1, ClassFlags::Unknown2};
		checkFlagsAssigned(_classFlags, classFlagsValues, 19, "UClass::UClass(...)", "Class");
	}
	_withinReference = new UObjectReference(input);
	_configNameIndex = new UNameIndex(input);
	_componentsMap = new TMap<UNameIndex, UObjectReference>(input);
	_interfaces = new TMap<UObjectReference, UObjectReference>(input);
	_dllBindName = new UNameIndex(input);
	_defaultReference = new UObjectReference(input);
}

UClass::~UClass(){
	delete _componentsMap;
	delete _configNameIndex;
	delete _defaultReference;
	delete _dllBindName;
	delete _interfaces;
	delete _withinReference;
}


uint32_t const UClass::size() const{
	return UState::size() + sizeof(_classFlags) + _componentsMap->size() + _configNameIndex->size() + _defaultReference->size() + _dllBindName->size() + _interfaces->size() + _withinReference->size();
}


void const UClass::write(UPKWriter& output) const{
	UState::write(output);
	{
		uint32_t const classFlagsValues[] = {ClassFlags::Abstract, ClassFlags::Compiled, ClassFlags::Config, ClassFlags::Transient, ClassFlags::Parsed, ClassFlags::Localized, ClassFlags::RuntimeStatic, ClassFlags::NoExport, ClassFlags::Placeable, ClassFlags::PerObjectConfig, ClassFlags::NativeReplication, ClassFlags::EditInlineNew, ClassFlags::CollapseCategories, ClassFlags::ExportStructs, ClassFlags::Instanced, ClassFlags::CacheExempt_Hidden, ClassFlags::HideDropDown2, ClassFlags::Unknown1, ClassFlags::Unknown2};
		checkFlagsAssigned(_classFlags, classFlagsValues, 19, "UClass::write(...)", "Class");
	}
	output.write(_classFlags);
	_withinReference->write(output);
	_configNameIndex->write(output);
	_componentsMap->write(output);
	_interfaces->write(output);
	_dllBindName->write(output);
	_defaultReference->write(output);
}