#include "UState.h"

#include <string>


UState::UState(UPKReader& input, FPackageFileSummary const& summary, FObjectExport const& object):
	UStruct(input, summary, object){
	input.read(_probeMask);
	input.read(_labelTableOffset);
	input.read(_stateFlags);
	{
		uint32_t const stateFlagsValues[] = {StateFlags::Auto, StateFlags::Simulated};
		checkFlagsAssigned(_stateFlags, stateFlagsValues, 2, "UState::UState(...)", "State");
	}
	_stateMap = new TMap<UNameIndex, UObjectReference>(input);
}

UState::~UState(){
	delete _stateMap;
}


uint32_t const UState::size() const{
	return UStruct::size() + sizeof(_labelTableOffset) + sizeof(_probeMask) + sizeof(_stateFlags) + _stateMap->size();
}


void const UState::write(UPKWriter& output) const{
	UStruct::write(output);
	output.write(_probeMask);
	output.write(_labelTableOffset);
	{
		uint32_t const stateFlagsValues[] = {StateFlags::Auto, StateFlags::Simulated};
		checkFlagsAssigned(_stateFlags, stateFlagsValues, 2, "UState::write(...)", "State");
	}
	output.write(_stateFlags);
	_stateMap->write(output);
}