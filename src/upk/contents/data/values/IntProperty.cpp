#include "IntProperty.h"


IntProperty::IntProperty(UPKReader& input){
	input.read(_value);
}


uint32_t const IntProperty::size() const{
	return sizeof(_value);
}


void const IntProperty::write(UPKWriter& output) const{
	output.write(_value);
}


int32_t const IntProperty::propertySize(){
	return 4;
}