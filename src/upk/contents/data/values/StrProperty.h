#pragma once

#include "PropertyValue.h"
#include "../../types/FString.h"


class StrProperty : public PropertyValue{
	private:
		/*
			Text value.
		*/
		FString* _value;


	public:
		/*
			Creates a string property value.

			\param 'input' Input UPK file reader.
		*/
		StrProperty(UPKReader& input);

		~StrProperty();


		uint32_t const size() const override;


		void const write(UPKWriter& output) const override;
};