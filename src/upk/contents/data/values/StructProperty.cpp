#include "StructProperty.h"

#include <string>


StructProperty::StructProperty(UPKReader& input, int32_t const& propertySize):
	_dataSize(propertySize){
	// TODO
	_data = new uint8_t[_dataSize];
	for(int32_t i = 0; i < _dataSize; i++){
		input.read(_data[i]);
	}
}

StructProperty::~StructProperty(){
	delete[] _data;
}


uint32_t const StructProperty::size() const{
	return _dataSize;
}


void const StructProperty::write(UPKWriter& output) const{
	for(int32_t i = 0; i < _dataSize; i++){
		output.write(_data[i]);
	}
}