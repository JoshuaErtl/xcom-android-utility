#pragma once

#include "PropertyValue.h"


class StructProperty : public PropertyValue{
	private:
		/*
			Internal data.
		*/
		uint8_t* _data;

		/*
			Size of data structure.
		*/
		int32_t _dataSize;


	public:
		/*
			Creates an object property value.

			\param 'input' Input UPK file reader.
		*/
		StructProperty(UPKReader& input, int32_t const& propertySize);

		~StructProperty();


		uint32_t const size() const override;


		void const write(UPKWriter& output) const override;
};