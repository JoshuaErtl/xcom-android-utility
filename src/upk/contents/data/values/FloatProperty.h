#pragma once

#include "PropertyValue.h"


class FloatProperty : public PropertyValue{
	private:
		/*
			Floating point value.
		*/
		float _value;


	public:
		/*
			Creates a float property value.

			\param 'input' Input UPK file reader.
		*/
		FloatProperty(UPKReader& input);


		uint32_t const size() const override;


		void const write(UPKWriter& output) const override;


		/*
			Gets the standard size of a float property value.

			\return Standard size of a float property value.
		*/
		static int32_t const propertySize();
};