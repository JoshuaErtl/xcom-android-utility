#pragma once

#include "PropertyValue.h"
#include "../../UNameIndex.h"


class ByteProperty : public PropertyValue{
	private:
		/*
			Enumeration name index value.
		*/
		UNameIndex* _enumValue;


	public:
		/*
			Creates a byte property value.

			\param 'input' Input UPK file reader.
		*/
		ByteProperty(UPKReader& input);

		~ByteProperty();


		uint32_t const size() const override;


		void const write(UPKWriter& output) const override;


		/*
			Gets the standard size of a byte property value.

			\return Standard size of a byte property value.
		*/
		static int32_t const propertySize();
};