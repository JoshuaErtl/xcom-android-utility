#pragma once

#include "PropertyValue.h"


class RawProperty : public PropertyValue{
	private:
		/*
			'true' if single value is expected.
		*/
		bool _expected;

		/*
			Size of the property.
		*/
		int32_t _propertySize;

		/*
			Raw values.
		*/
		uint8_t* _values;


	public:
		/*
			Creates a raw array property value.

			\param 'input' Input UPK file reader.
			\param 'propertySize' Size of property.
		*/
		RawProperty(UPKReader& input, int32_t const& propertySize);
		/*
			Creates a raw array property value with an expected input.

			\param 'input' Input UPK file reader.
			\param 'propertySize' Size of property.
			\param 'expectedValue' Expected value to read.
		*/
		RawProperty(UPKReader& input, int32_t const& propertySize, uint8_t expectedValue);

		~RawProperty();


		uint32_t const size() const override;


		void const write(UPKWriter& output) const override;
};