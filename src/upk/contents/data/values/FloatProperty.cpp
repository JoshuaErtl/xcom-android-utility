#include "FloatProperty.h"


FloatProperty::FloatProperty(UPKReader& input){
	input.read(_value);
}


uint32_t const FloatProperty::size() const{
	return sizeof(_value);
}


void const FloatProperty::write(UPKWriter& output) const{
	output.write(_value);
}


int32_t const FloatProperty::propertySize(){
	return 4;
}