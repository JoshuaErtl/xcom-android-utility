#pragma once

#include "PropertyValue.h"


class IntProperty : public PropertyValue{
	private:
		/*
			Integer value.
		*/
		int32_t _value;


	public:
		/*
			Creates an integer property value.

			\param 'input' Input UPK file reader.
		*/
		IntProperty(UPKReader& input);


		uint32_t const size() const override;


		void const write(UPKWriter& output) const override;


		/*
			Gets the standard size of an integer property value.

			\return Standard size of an integer property value.
		*/
		static int32_t const propertySize();
};