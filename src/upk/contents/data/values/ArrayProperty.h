#pragma once

#include "PropertyValue.h"


class ArrayProperty : public PropertyValue{
	private:
		/*
			Internal data.
		*/
		uint8_t* _data;

		/*
			Size of data array.
		*/
		int32_t _dataSize;

		/*
			Number of array elements.
		*/
		int32_t _numElements;


	public:
		/*
			Creates an array property value.

			\param 'input' Input UPK file reader.
			\param 'propertySize' Size of property.
		*/
		ArrayProperty(UPKReader& input, int32_t const& propertySize);

		~ArrayProperty();


		uint32_t const size() const override;


		void const write(UPKWriter& output) const override;
};