#pragma once

#include "PropertyValue.h"
#include "../../UNameIndex.h"


class DelegateProperty : public PropertyValue{
	private:
		/*
			Name of function to delegate to.
		*/
		UNameIndex* _function;


	public:
		/*
			Creates a delegate property value.

			\param 'input' Input UPK file reader.
		*/
		DelegateProperty(UPKReader& input);

		~DelegateProperty();


		uint32_t const size() const override;


		void const write(UPKWriter& output) const override;


		/*
			Gets the standard size of a delegate property value.

			\return Standard size of a delegate property value.
		*/
		static int32_t const propertySize();
};