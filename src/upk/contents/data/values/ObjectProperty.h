#pragma once

#include "PropertyValue.h"
#include "../../UObjectReference.h"


class ObjectProperty : public PropertyValue{
	private:
		/*
			Object reference.
		*/
		UObjectReference* _object;


	public:
		/*
			Creates an object property value.

			\param 'input' Input UPK file reader.
		*/
		ObjectProperty(UPKReader& input);

		~ObjectProperty();


		uint32_t const size() const override;


		void const write(UPKWriter& output) const override;


		/*
			Gets the standard size of an object property value.

			\return Standard size of an object property value.
		*/
		static int32_t const propertySize();
};