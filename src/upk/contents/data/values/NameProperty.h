#pragma once

#include "PropertyValue.h"
#include "../../UNameIndex.h"


class NameProperty : public PropertyValue{
	private:
		/*
			Name index value.
		*/
		UNameIndex* _nameValue;


	public:
		/*
			Creates a name property value.

			\param 'input' Input UPK file reader.
		*/
		NameProperty(UPKReader& input);

		~NameProperty();


		uint32_t const size() const override;


		void const write(UPKWriter& output) const override;


		/*
			Gets the standard size of a name property value.

			\return Standard size of a name property value.
		*/
		static int32_t const propertySize();
};