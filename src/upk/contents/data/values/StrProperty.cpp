#include "StrProperty.h"


StrProperty::StrProperty(UPKReader& input){
	_value = new FString(input);
}

StrProperty::~StrProperty(){
	delete _value;
}


uint32_t const StrProperty::size() const{
	return _value->size();
}


void const StrProperty::write(UPKWriter& output) const{
	_value->write(output);
}