#include "ByteProperty.h"


ByteProperty::ByteProperty(UPKReader& input){
	_enumValue = new UNameIndex(input);
}

ByteProperty::~ByteProperty(){
	delete _enumValue;
}


uint32_t const ByteProperty::size() const{
	return _enumValue->size();
}


void const ByteProperty::write(UPKWriter& output) const{
	_enumValue->write(output);
}


int32_t const ByteProperty::propertySize(){
	return 8;
}