#include "NameProperty.h"


NameProperty::NameProperty(UPKReader& input){
	_nameValue = new UNameIndex(input);
}

NameProperty::~NameProperty(){
	delete _nameValue;
}


uint32_t const NameProperty::size() const{
	return _nameValue->size();
}


void const NameProperty::write(UPKWriter& output) const{
	_nameValue->write(output);
}


int32_t const NameProperty::propertySize(){
	return 8;
}