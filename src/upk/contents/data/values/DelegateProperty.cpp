#include "DelegateProperty.h"

#include <string>


DelegateProperty::DelegateProperty(UPKReader& input){
	int32_t temp;
	input.read(temp);
	if(temp != 0){
		throw std::runtime_error(std::string("DelegateProperty::DelegateProperty(...) : Value not 0!\n\t(Value: ").append(std::to_string(temp)).append(")"));
	}
	_function = new UNameIndex(input);
}

DelegateProperty::~DelegateProperty(){
	delete _function;
}


uint32_t const DelegateProperty::size() const{
	return sizeof(int32_t) + _function->size();
}


void const DelegateProperty::write(UPKWriter& output) const{
	output.write((int32_t) 0);
	_function->write(output);
}


int32_t const DelegateProperty::propertySize(){
	return 12;
}