#include "ObjectProperty.h"


ObjectProperty::ObjectProperty(UPKReader& input){
	_object = new UObjectReference(input);
}

ObjectProperty::~ObjectProperty(){
	delete _object;
}


uint32_t const ObjectProperty::size() const{
	return _object->size();
}


void const ObjectProperty::write(UPKWriter& output) const{
	_object->write(output);
}


int32_t const ObjectProperty::propertySize(){
	return 4;
}