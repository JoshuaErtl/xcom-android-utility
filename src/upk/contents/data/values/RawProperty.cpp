#include "RawProperty.h"

#include <string>


RawProperty::RawProperty(UPKReader& input, int32_t const& propertySize):
	_expected(false),
	_propertySize(propertySize){
	_values = new uint8_t[propertySize];
	for(int32_t i = 0; i < propertySize; i++){
		input.read(_values[i]);
	}
}
RawProperty::RawProperty(UPKReader& input, int32_t const& propertySize, uint8_t expectedValue):
	_expected(true),
	_propertySize(propertySize){
	uint8_t temp;
	for(int32_t i = 0; i < propertySize; i++){
		input.read(temp);
		if(temp != expectedValue){
			throw std::runtime_error(std::string("RawProperty::RawProperty(..., ").append(std::to_string(propertySize)).append(", ").append(std::to_string(expectedValue)).append(") : Value does not match expected!\n\t(Index: ").append(std::to_string(i)).append("  Value: ").append(std::to_string(temp)).append(")"));
		}
	}
	_values = new uint8_t{expectedValue};
}

RawProperty::~RawProperty(){
	delete[] _values;
}


uint32_t const RawProperty::size() const{
	return _propertySize;
}


void const RawProperty::write(UPKWriter& output) const{
	if(_expected){
		for(int32_t _ = 0; _ < _propertySize; _++){
			output.write(_values[0]);
		}
	}
	else{
		for(int32_t i = 0; i < _propertySize; i++){
			output.write(_values[i]);
		}
	}
}