#include "ArrayProperty.h"


ArrayProperty::ArrayProperty(UPKReader& input, int32_t const& propertySize):
	_dataSize(propertySize - 4){
	input.read(_numElements);
	// TODO
	_data = new uint8_t[_dataSize];
	for(int32_t i = 0; i < _dataSize; i++){
		input.read(_data[i]);
	}
}

ArrayProperty::~ArrayProperty(){
	delete[] _data;
}


uint32_t const ArrayProperty::size() const{
	return _dataSize + sizeof(_numElements);
}


void const ArrayProperty::write(UPKWriter& output) const{
	output.write(_numElements);
	for(int32_t i = 0; i < _dataSize; i++){
		output.write(_data[i]);
	}
}