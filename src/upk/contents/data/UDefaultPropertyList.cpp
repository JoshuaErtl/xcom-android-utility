#include "UDefaultPropertyList.h"


UDefaultPropertyList::UDefaultPropertyList(UPKReader& input, FPackageFileSummary const& summary){
	UNameIndex* nameIndex;
	while((nameIndex = new UNameIndex(input))->toString(summary) != "None"){
		_list.emplace_back(new UDefaultProperty(input, nameIndex, summary));
	}
	_noneIndex = nameIndex;
}

UDefaultPropertyList::~UDefaultPropertyList(){
	for(std::vector<UDefaultProperty*>::const_iterator it = _list.begin(); it != _list.end(); it++){
		delete *it;
	}
	delete _noneIndex;
}


uint32_t const UDefaultPropertyList::size() const{
	uint32_t size = _noneIndex->size();
	for(std::vector<UDefaultProperty*>::const_iterator it = _list.begin(); it != _list.end(); it++){
		size += (*it)->size();
	}
	return size;
}


void const UDefaultPropertyList::write(UPKWriter& output) const{
	for(std::vector<UDefaultProperty*>::const_iterator it = _list.begin(); it != _list.end(); it++){
		(*it)->write(output);
	}
	_noneIndex->write(output);
}