//#include "UObjectReference.h"

#include "FPackageFileSummary.h"


UObjectReference const UObjectReference::NULL_REFERENCE(0);


UObjectReference::UObjectReference(int32_t const& index):
	_objectReference(index){}

UObjectReference::UObjectReference(UPKReader& input){
	input.read(_objectReference);
}


FObjectBase* const UObjectReference::object(FPackageFileSummary const& summary) const{
	if(_objectReference == 0){
		return nullptr;
	}
	else if(_objectReference > 0){
		return summary.getExport(_objectReference);
	}
	return summary.getImport(_objectReference);
}

uint32_t const UObjectReference::size() const{
	return sizeof(_objectReference);
}


bool const UObjectReference::equals(UObjectReference const& o) const{
	return _objectReference == o._objectReference;
}


void const UObjectReference::write(UPKWriter& output) const{
	output.write(_objectReference);
}