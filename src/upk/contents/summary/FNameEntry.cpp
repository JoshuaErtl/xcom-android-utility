#include "FNameEntry.h"


FNameEntry::FNameEntry(UPKReader& input){
	_name = new FString(input);
	input.read(_nameFlagsH);
	{
		uint32_t const nameFlagsHValues[1] = {};
		checkFlagsAssigned(_nameFlagsH, nameFlagsHValues, 0, "FNameEntry::FNameEntry(...)", "Name high");
	}
	input.read(_nameFlagsL);
	{
		uint32_t const nameFlagsLValues[] = {NameFlagsL::Unknown1, NameFlagsL::Unknown5, NameFlagsL::Unknown2, NameFlagsL::Unknown3, NameFlagsL::Unknown4};
		checkFlagsAssigned(_nameFlagsL, nameFlagsLValues, 5, "FNameEntry::FNameEntry(...)", "Name low");
	}
}

FNameEntry::~FNameEntry(){
	delete _name;
}


uint32_t const FNameEntry::size() const{
	return _name->size() + sizeof(_nameFlagsH) + sizeof(_nameFlagsL);
}


std::string FNameEntry::toString() const{
	return _name->toString();
}

void const FNameEntry::write(UPKWriter& output) const{
	_name->write(output);
	{
		uint32_t const nameFlagsHValues[1] = {};
		checkFlagsAssigned(_nameFlagsH, nameFlagsHValues, 0, "FNameEntry::write(...)", "Name high");
	}
	output.write(_nameFlagsH);
	{
		uint32_t const nameFlagsLValues[] = {NameFlagsL::Unknown1, NameFlagsL::Unknown5, NameFlagsL::Unknown2, NameFlagsL::Unknown3, NameFlagsL::Unknown4};
		checkFlagsAssigned(_nameFlagsL, nameFlagsLValues, 5, "FNameEntry::write(...)", "Name low");
	}
	output.write(_nameFlagsL);
}