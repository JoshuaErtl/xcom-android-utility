#pragma once

#include <vector>

#include "../types/FGuid.h"
#include "FObjectBase.h"
#include "../data/UObject.h"


class FObjectExport : public FObjectBase{
	public:
		enum ExportFlags: uint32_t{
			ForcedExport	= 0x00000001 // TODO: Forced export?
		};
		enum ObjectFlagsH: uint32_t{
			PerObjectLocalized	= 0x00000100, // TODO: Per object localized
			PropertiesObject	= 0x00000200, // TODO: Properties object?
			ArchetypeObject		= 0x00000400 // TODO: Archetype object
		};
		enum ObjectFlagsL: uint32_t{
			Transactional	= 0x00000001, // TODO: Transactional?
			Public			= 0x00000004, // Visible outside its package
			Automated		= 0x00000100, // TODO: Automated?
			LoadForClient	= 0x00010000, // In-file load for client
			LoadForServer	= 0x00020000, // In-file load for server
			LoadForEdit		= 0x00040000, // In-file load for editor
			Standalone		= 0x00080000, // Keep object around for editing even if unreferenced
			NotForClient	= 0x00100000, // Don't load for client
			NotForServer	= 0x00200000, // Don't load for server
			HasStack		= 0x02000000, // Has execution stack
			Native			= 0x04000000 // Native implementation (UClass only)
		};


	private:
		/*
			Archetype of the object.
		*/
		UObjectReference* _archetypeReference;

		/*
			External data of the object as a structure.
		*/
		UObject* _dataObject = nullptr;
		/*
			External data of the object as raw values.
		*/
		uint8_t* _dataRaw = nullptr;

		/*
			Indicator for properties of exporting the object.
		*/
		int32_t _exportFlags;

		/*
			Global unique identifier for the net objects.
		*/
		FGuid* _guid;

		/*
			//TODO: Net objects?
		*/
		std::vector<int32_t> _netObjects;

		/*
			Indicator for properties of the object.
		*/
		int32_t _objectFlagsH;
		/*
			Indicator for properties of the object.
		*/
		int32_t _objectFlagsL;

		/*
			Parent class of the object.
		*/
		UObjectReference* _parentClassReference;

		/*
			Offset in file to find object's external data.
		*/
		int32_t _serialOffset;
		/*
			Size of external data.
		*/
		int32_t _serialSize;

		/*
			Type of the object.
		*/
		UObjectReference* _typeReference;

		/*
			//TODO: Unknown net object data?
		*/
		int32_t _unknown1;


	public:
		/*
			Creates an export object entry.

			\param 'input' Input UPK file reader.
			\param 'summary' Summary of UPK file.
		*/
		FObjectExport(UPKReader& input, FPackageFileSummary const& summary);

		~FObjectExport();


		/*
			Gets the size of the object's external data.

			\return Size of the object's external data.
		*/
		uint32_t const dataSize() const;

		uint32_t const size() const override;

		std::string typeName() const override;


		/*
			Checks if the object has a high flag property.

			\param 'flag' High flag to check.

			\return 'true' if the object has the flag.
		*/
		bool const hasFlag(ObjectFlagsH const& flag) const;
		/*
			Checks if the object has a low flag property.

			\param 'flag' Low flag to check.

			\return 'true' if the object has the flag.
		*/
		bool const hasFlag(ObjectFlagsL const& flag) const;


		/*
			Reads object's external data from UPK file.

			\param 'input' Input UPK file reader.
		*/
		void const readData(UPKReader& input);
		/*
			Writes object's external data to UPK file.

			\param 'output' Output UPK file writer.
		*/
		void const writeData(UPKWriter& output) const;

		/*
			Unpacks raw object data into data structures.
		*/
		void const unpackRaw();

		void const write(UPKWriter& output) const override;
		/*
			Writes export object data to UPK file with new serial data offset.

			\param 'output' Output UPK file writer.
			\param 'serialOffset' New offset for object data.
		*/
		void const write(UPKWriter& output, int32_t const& serialOffset);
};