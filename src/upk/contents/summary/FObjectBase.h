#pragma once

#include "../UNameIndex.h"
#include "../UObjectReference.h"


class FObjectBase : public Object{
	protected:
		/*
			Summary of UPK file.
		*/
		FPackageFileSummary const* const _summary;


		/*
			Name of the object.
		*/
		UNameIndex* _nameIndex;

		/*
			Owner of the object.
		*/
		UObjectReference* _ownerReference;


		/*
			Creates a base object entry.

			\param 'summary' Summary of UPK file.
		*/
		FObjectBase(FPackageFileSummary const& summary);

		~FObjectBase();


		/*
			Reads data for a base object entry.

			\param 'input' Input UPK file reader.
		*/
		void const readBase(UPKReader& input);


	public:
		/*
			Gets the full name of the object.

			\return Full name of the object.
		*/
		std::string fullName() const;

		uint32_t const size() const override;

		/*
			Gets the type name of the object.

			\return Type name of the object.
		*/
		virtual std::string typeName() const = 0;


		void const write(UPKWriter& output) const override;
};