#include "FCompressedChunk.h"


FCompressedChunk::FCompressedChunk(UPKReader& input){
	input.read(_uncompressedOffset);
	input.read(_uncompressedSize);
	input.read(_compressedOffset);
	input.read(_compressedSize);
	//TODO: Handle data
	throw std::runtime_error("FCompressedChunk::FCompressedChunk(...) : Handle FCompressedChunk data!");
}


uint32_t const FCompressedChunk::size() const{
	return sizeof(_compressedOffset) + sizeof(_compressedSize) + sizeof(_uncompressedOffset) + sizeof(_uncompressedSize);
}


void const FCompressedChunk::write(UPKWriter& output) const{
	output.write(_uncompressedOffset);
	output.write(_uncompressedSize);
	output.write(_compressedOffset);
	output.write(_compressedSize);
}