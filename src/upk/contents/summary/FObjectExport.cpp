//#include "FObjectExport.h"

#include <string>

#include "../data/properties/UArrayProperty.h"
#include "../data/properties/UBoolProperty.h"
#include "../data/properties/UByteProperty.h"
#include "../data/properties/UClassProperty.h"
#include "../data/properties/UComponentProperty.h"
#include "../data/properties/UDelegateProperty.h"
#include "../data/properties/UFloatProperty.h"
#include "../data/properties/UInterfaceProperty.h"
#include "../data/properties/UIntProperty.h"
#include "../data/properties/UNameProperty.h"
#include "../data/properties/UObjectProperty.h"
#include "../data/properties/UStrProperty.h"
#include "../data/properties/UStructProperty.h"
#include "../data/ClassObject.h"
#include "../data/UClass.h"
#include "../data/UConst.h"
#include "../data/UEnum.h"
#include "../data/UFunction.h"
#include "../data/UScriptStruct.h"
#include "../FPackageFileSummary.h"
#include "../../UPKReaderMemory.h"


FObjectExport::FObjectExport(UPKReader& input, FPackageFileSummary const& summary):
	FObjectBase(summary){
	_typeReference = new UObjectReference(input);
	_parentClassReference = new UObjectReference(input);
	readBase(input);
	_archetypeReference = new UObjectReference(input);
	input.read(_objectFlagsH);
	{
		uint32_t const objectFlagsHValues[] = {ObjectFlagsH::PerObjectLocalized, ObjectFlagsH::PropertiesObject, ObjectFlagsH::ArchetypeObject};
		checkFlagsAssigned(_objectFlagsH, objectFlagsHValues, 3, "FObjectExport::FObjectExport(...)", "Object high");
	}
	input.read(_objectFlagsL);
	{
		uint32_t const objectFlagsLValues[] = {ObjectFlagsL::Transactional, ObjectFlagsL::Public, ObjectFlagsL::Automated, ObjectFlagsL::LoadForClient, ObjectFlagsL::LoadForServer, ObjectFlagsL::LoadForEdit, ObjectFlagsL::Standalone, ObjectFlagsL::NotForClient, ObjectFlagsL::NotForServer, ObjectFlagsL::HasStack, ObjectFlagsL::Native};
		checkFlagsAssigned(_objectFlagsL, objectFlagsLValues, 11, "FObjectExport::FObjectExport(...)", "Object low");
	}
	input.read(_serialSize);
	input.read(_serialOffset);
	input.read(_exportFlags);
	{
		uint32_t const exportFlagsValues[] = {ExportFlags::ForcedExport};
		checkFlagsAssigned(_exportFlags, exportFlagsValues, 1, "FObjectExport::FObjectExport(...)", "Export");
	}
	{
		int32_t netObjectCount;
		input.read(netObjectCount);
		_netObjects.resize(netObjectCount);
		int32_t temp;
		for(int32_t i = 0; i < netObjectCount; i++){
			input.read(temp);
			_netObjects.at(i) = temp;
		}
		_guid = new FGuid(input);
		{
			bool const isZero = netObjectCount == 0;
			if(isZero != _guid->equals(FGuid::ZERO)){
				throw std::runtime_error(std::string("FObjectExport::FObjectExport(...) : ").append(isZero ? "GUID is not zero!" : "GUID is not unique!").append("\n\t(GUID: ").append(_guid->toString()).append(")"));
			}
		}
	}
	input.read(_unknown1);
}

FObjectExport::~FObjectExport(){
	delete _archetypeReference;
	if(_dataObject){
		delete _dataObject;
	}
	if(_dataRaw){
		delete[] _dataRaw;
	}
	delete _guid;
	delete _parentClassReference;
	delete _typeReference;
}


uint32_t const FObjectExport::dataSize() const{
	return _dataObject->size();
}

uint32_t const FObjectExport::size() const{
	return FObjectBase::size() + sizeof(int32_t) + _archetypeReference->size() + sizeof(_exportFlags) + _guid->size() + (sizeof(int32_t) * _netObjects.size()) + sizeof(_objectFlagsH) + sizeof(_objectFlagsL) + _parentClassReference->size() + sizeof(_serialOffset) + sizeof(_serialSize) + _typeReference->size() + sizeof(_unknown1);
}

std::string FObjectExport::typeName() const{
	FObjectBase const* const type = _typeReference->object(*_summary);
	if(type){
		return type->fullName();
	}
	return "Core.Class";
}


bool const FObjectExport::hasFlag(ObjectFlagsH const& flag) const{
	return (_objectFlagsH & flag) > 0;
}
bool const FObjectExport::hasFlag(ObjectFlagsL const& flag) const{
	return (_objectFlagsL & flag) > 0;
}


void const FObjectExport::readData(UPKReader& input){
	_dataRaw = new uint8_t[_serialSize];
	for(int32_t i = 0; i < _serialSize; i++){
		input.read(_dataRaw[i]);
	}
}
void const FObjectExport::writeData(UPKWriter& output) const{
	if(_dataObject){
		_dataObject->write(output);
	}
	else{
		for(int32_t i = 0; i < _serialSize; i++){
			output.write(_dataRaw[i]);
		}
	}
}

void const FObjectExport::unpackRaw(){
	UPKReaderMemory input(_dataRaw, _serialSize, _serialOffset);
	std::string type = typeName();
	if(type == "Core.ScriptStruct"){
		_dataObject = new UScriptStruct(input, *_summary, *this);
	}
	else if(type == "Core.Function"){
		_dataObject = new UFunction(input, *_summary, *this);
	}
	else if(type == "Core.State"){
		_dataObject = new UState(input, *_summary, *this);
	}
	else if(type == "Core.Class"){
		_dataObject = new UClass(input, *_summary, *this);
	}
	else if(type == "Core.Const"){
		_dataObject = new UConst(input, *_summary, *this);
	}
	else if(type == "Core.Enum"){
		_dataObject = new UEnum(input, *_summary, *this);
	}
	else if(type == "Core.ByteProperty"){
		_dataObject = new UByteProperty(input, *_summary, *this);
	}
	else if(type == "Core.IntProperty"){
		_dataObject = new UIntProperty(input, *_summary, *this);
	}
	else if(type == "Core.BoolProperty"){
		_dataObject = new UBoolProperty(input, *_summary, *this);
	}
	else if(type == "Core.FloatProperty"){
		_dataObject = new UFloatProperty(input, *_summary, *this);
	}
	else if(type == "Core.ObjectProperty"){
		_dataObject = new UObjectProperty(input, *_summary, *this);
	}
	else if(type == "Core.ClassProperty"){
		_dataObject = new UClassProperty(input, *_summary, *this);
	}
	else if(type == "Core.ComponentProperty"){
		_dataObject = new UComponentProperty(input, *_summary, *this);
	}
	else if(type == "Core.NameProperty"){
		_dataObject = new UNameProperty(input, *_summary, *this);
	}
	else if(type == "Core.StructProperty"){
		_dataObject = new UStructProperty(input, *_summary, *this);
	}
	else if(type == "Core.StrProperty"){
		_dataObject = new UStrProperty(input, *_summary, *this);
	}
	else if(type == "Core.ArrayProperty"){
		_dataObject = new UArrayProperty(input, *_summary, *this);
	}
	else if(type == "Core.DelegateProperty"){
		_dataObject = new UDelegateProperty(input, *_summary, *this);
	}
	else if(type == "Core.InterfaceProperty"){
		_dataObject = new UInterfaceProperty(input, *_summary, *this);
	}
	else if(hasFlag(ObjectFlagsH::PropertiesObject)){
		_dataObject = new UObject(input, *_summary, *this);
	}
	else{
		_dataObject = new ClassObject(input, *_summary, *this, _serialSize);
		//throw std::runtime_error(std::string("FObjectExport::unpackRaw() : Unhandled type!\n\t(Type: ").append(type).append(")"));
	}
	uint32_t temp = _dataObject->size();
	if(temp != _serialSize){
		throw std::runtime_error(std::string("FObjectExport::unpackRaw() : Data size does not match serial size!\n\t(Data Size: ").append(std::to_string(temp)).append("  Serial Size: ").append(std::to_string(_serialSize)).append(")"));
	}
	delete[] _dataRaw;
	_dataRaw = nullptr;
}

void const FObjectExport::write(UPKWriter& output) const{
	_typeReference->write(output);
	_parentClassReference->write(output);
	FObjectBase::write(output);
	_archetypeReference->write(output);
	{
		uint32_t const objectFlagsHValues[] = {ObjectFlagsH::PerObjectLocalized, ObjectFlagsH::PropertiesObject, ObjectFlagsH::ArchetypeObject};
		checkFlagsAssigned(_objectFlagsH, objectFlagsHValues, 3, "FObjectExport::write(...)", "Object high");
	}
	output.write(_objectFlagsH);
	{
		uint32_t const objectFlagsLValues[] = {ObjectFlagsL::Transactional, ObjectFlagsL::Public, ObjectFlagsL::Automated, ObjectFlagsL::LoadForClient, ObjectFlagsL::LoadForServer, ObjectFlagsL::LoadForEdit, ObjectFlagsL::Standalone, ObjectFlagsL::NotForClient, ObjectFlagsL::NotForServer, ObjectFlagsL::HasStack, ObjectFlagsL::Native};
		checkFlagsAssigned(_objectFlagsL, objectFlagsLValues, 11, "FObjectExport::write(...)", "Object low");
	}
	output.write(_objectFlagsL);
	output.write(_dataObject ? (int32_t) _dataObject->size() : _serialSize);
	output.write(_serialOffset);
	{
		uint32_t const exportFlagsValues[] = {ExportFlags::ForcedExport};
		checkFlagsAssigned(_exportFlags, exportFlagsValues, 1, "FObjectExport::write(...)", "Export");
	}
	output.write(_exportFlags);
	int32_t const netObjectCount = _netObjects.size();
	output.write(netObjectCount);
	for(std::vector<int32_t>::const_iterator it = _netObjects.begin(); it != _netObjects.end(); it++){
		output.write(*it);
	}
	_guid->write(output);
	{
		bool const isZero = netObjectCount == 0;
		if(isZero != _guid->equals(FGuid::ZERO)){
			throw std::runtime_error(std::string("FObjectExport::write(...) : ").append(isZero ? "GUID is not zero!" : "GUID is not unique!").append("\n\t(GUID: ").append(_guid->toString()).append(")"));
		}
	}
	output.write(_unknown1);
}
void const FObjectExport::write(UPKWriter& output, int32_t const& serialOffset){
	_serialOffset = serialOffset;
	write(output);
}