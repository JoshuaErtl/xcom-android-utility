#pragma once

#include "FObjectBase.h"


class FObjectImport : public FObjectBase{
	private:
		/*
			Name of the containing package.
		*/
		UNameIndex* _packageIndex;

		/*
			Type of the object.
		*/
		UNameIndex* _typeIndex;


	public:
		/*
			Creates an import object entry.

			\param 'input' Input UPK file reader.
			\param 'summary' Summary of UPK file.
		*/
		FObjectImport(UPKReader& input, FPackageFileSummary const& summary);

		~FObjectImport();


		uint32_t const size() const override;

		std::string typeName() const override;


		void const write(UPKWriter& output) const override;
};