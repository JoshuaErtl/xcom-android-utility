#include "FObjectBase.h"

#include "../FPackageFileSummary.h"


FObjectBase::FObjectBase(FPackageFileSummary const& summary):
	_summary(&summary){}

FObjectBase::~FObjectBase(){
	delete _nameIndex;
	delete _ownerReference;
}


std::string FObjectBase::fullName() const{
	FObjectBase const* const owner = _ownerReference->object(*_summary);
	if(owner){
		return owner->fullName().append(".").append(_nameIndex->toString(*_summary));
	}
	return _nameIndex->toString(*_summary);
}

uint32_t const FObjectBase::size() const{
	return _nameIndex->size() + _ownerReference->size();
}


void const FObjectBase::readBase(UPKReader& input){
	_ownerReference = new UObjectReference(input);
	_nameIndex = new UNameIndex(input);
}

void const FObjectBase::write(UPKWriter& output) const{
	_ownerReference->write(output);
	_nameIndex->write(output);
}