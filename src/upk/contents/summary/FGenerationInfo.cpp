#include "FGenerationInfo.h"


FGenerationInfo::FGenerationInfo(UPKReader& input){
	input.read(_exportCount);
	input.read(_nameCount);
	input.read(_netObjectCount);
}


uint32_t const FGenerationInfo::size() const{
	return sizeof(_exportCount) + sizeof(_nameCount) + sizeof(_netObjectCount);
}


void const FGenerationInfo::write(UPKWriter& output) const{
	output.write(_exportCount);
	output.write(_nameCount);
	output.write(_netObjectCount);
}