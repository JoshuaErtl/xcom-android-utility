#pragma once

#include "../types/FString.h"


class FNameEntry : public Object{
	public:
		enum NameFlagsL: uint32_t{
			Unknown1	= 0x00000010, // TODO: Unknown?
			Unknown5	= 0x00001000, // TODO: Unknown?
			Unknown2	= 0x00010000, // TODO: Unknown?
			Unknown3	= 0x00020000, // TODO: Unknown?
			Unknown4	= 0x00040000 // TODO: Unknown?
		};


	private:
		/*
			Text name of the entry.
		*/
		FString* _name;

		/*
			Indicator for properties of the name.
		*/
		int32_t _nameFlagsH;
		/*
			Indicator for properties of the name.
		*/
		int32_t _nameFlagsL;


	public:
		/*
			Creates a name entry.

			\param 'input' Input UPK file reader.
		*/
		FNameEntry(UPKReader& input);

		~FNameEntry();


		uint32_t const size() const override;


		/*
			Converts the name entry to a string.

			\return String of the name entry.
		*/
		std::string toString() const;

		void const write(UPKWriter& output) const override;
};