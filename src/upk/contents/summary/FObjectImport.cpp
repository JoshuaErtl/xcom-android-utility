//#include "FObjectImport.h"

#include "../FPackageFileSummary.h"


FObjectImport::FObjectImport(UPKReader& input, FPackageFileSummary const& summary):
	FObjectBase(summary){
	_packageIndex = new UNameIndex(input);
	_typeIndex = new UNameIndex(input);
	readBase(input);
}

FObjectImport::~FObjectImport(){
	delete _packageIndex;
	delete _typeIndex;
}


uint32_t const FObjectImport::size() const{
	return FObjectBase::size() + _packageIndex->size() + _typeIndex->size();
}

std::string FObjectImport::typeName() const{
	return _packageIndex->toString(*_summary).append(".").append(_typeIndex->toString(*_summary));
}


void const FObjectImport::write(UPKWriter& output) const{
	_packageIndex->write(output);
	_typeIndex->write(output);
	FObjectBase::write(output);
}