#pragma once

#include "../Object.h"


class FCompressedChunk : public Object{
	private:
		/*
			Offset in file to find compressed data.
		*/
		int32_t _compressedOffset;
		/*
			Size of compressed data.
		*/
		int32_t _compressedSize;

		/*
			Offset in file to find uncompressed data.
		*/
		int32_t _uncompressedOffset;
		/*
			Size of uncompressed data.
		*/
		int32_t _uncompressedSize;


	public:
		/*
			Creates a compressed chunk of data.

			\param 'input' Input UPK file reader.
		*/
		FCompressedChunk(UPKReader& input);


		uint32_t const size() const override;


		void const write(UPKWriter& output) const override;
};