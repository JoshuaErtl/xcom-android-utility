#pragma once

#include "../Object.h"


class FGenerationInfo : public Object{
	private:
		/*
			Number of previous entries in the export table.
		*/
		int32_t _exportCount;
		/*
			Number of previous entries in the name table.
		*/
		int32_t _nameCount;
		/*
			Number of previous net objects.
		*/
		int32_t _netObjectCount;


	public:
		/*
			Creates a generation record.

			\param 'input' Input UPK file reader.
		*/
		FGenerationInfo(UPKReader& input);


		uint32_t const size() const override;


		void const write(UPKWriter& output) const override;
};