#pragma once

#include "Object.h"


class UNameIndex : public Object{
	private:
		/*
			Index for entry in name table.
		*/
		int32_t _nameTableIndex;

		/*
			Numeric addition to text.
		*/
		int32_t _numeric;


	public:
		/*
			Creates a name label.

			\param 'input' Input UPK file reader.
		*/
		UNameIndex(UPKReader& input);


		uint32_t const size() const override;


		/*
			Converts the name label to a string.

			\param 'summary' Summary of UPK file.

			\return String of the name label.
		*/
		std::string toString(FPackageFileSummary const& summary) const;

		void const write(UPKWriter& output) const override;
};