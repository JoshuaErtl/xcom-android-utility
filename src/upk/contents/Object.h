#pragma once

#include "../UPKReader.h"
#include "../UPKWriter.h"
#include "../../FileWriter.h"


class FPackageFileSummary;


class Object{
	public:
		/*
			Gets the size of the object.

			\return Size of the object.
		*/
		virtual uint32_t const size() const = 0;


		/*
			Writes object data to file as data dump.

			\param 'output' Output file writer.
			\param 'indent' Number of indents.
		*/
		virtual void const dump(FileWriter& output, uint8_t const& indent = 0) const = 0;

		/*
			Writes object data to UPK file.

			\param 'output' Output UPK file writer.
		*/
		virtual void const write(UPKWriter& output) const = 0;


		/*
			Checks the flags of an object for existence.  If a flag exists on
			the object but is not assigned to the enumeration values, a
			'runtime_error' exception is thrown.

			\param 'flags' Flags of the object.
			\param 'enums' Enumeration values to check.
			\param 'enumsSize' Number of enumeration values.
			\param 'functionString' Header of the function that made this call.
			\param 'flagsString' Name of the flags.
		*/
		static void const checkFlagsAssigned(uint32_t flags, uint32_t const* const& enums, int32_t const& enumsSize, char const* const& functionString, char const* const& flagsString);
};