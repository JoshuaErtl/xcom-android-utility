#include "Object.h"


void const Object::checkFlagsAssigned(uint32_t flags, uint32_t const* const& enums, int32_t const& enumsSize, char const* const& functionString, char const* const& flagsString){
	for(int32_t i = 0; i < enumsSize; i++){
		flags &= ~enums[i];
	}
	if(flags != 0){
		throw std::runtime_error(std::string(functionString).append(" : ").append(flagsString).append(" flags has unassigned values!\n\t(Remaining Flags: ").append(UPKReader::toHexString(&flags, sizeof(flags))).append(")"));
	}
}