#include "FPackageFileSummary.h"

#include <string>


FPackageFileSummary::FPackageFileSummary(UPKReader& input){
	input.read(_signature);
	if(_signature != 0x9E2A83C1){
		throw std::runtime_error(std::string("FPackageFileSummary::FPackageFileSummary(...) : Invalid signature!\n\t(Signature: ").append(UPKReader::toHexString(&_signature, sizeof(_signature))).append(")"));
	}
	input.read(_version);
	if((_version & 0xFFFF) != 870){
		throw std::runtime_error(std::string("FPackageFileSummary::FPackageFileSummary(...) : Package version not mobile value!\n\t(Package Version: ").append(std::to_string(_version & 0xFFFF)).append("  Mobile Package Version: 870)"));
	}
	if(_version >> 16 != 64){
		throw std::runtime_error(std::string("FPackageFileSummary::FPackageFileSummary(...) : Licensee version not mobile value!\n\t(Licensee Version: ").append(std::to_string(_version >> 16)).append("  Mobile Licensee Version: 64)"));
	}
	int32_t headerSize;
	input.read(headerSize);
	_packageGroup = new FString(input);
	if(!_packageGroup->equals("None")){
		throw std::runtime_error(std::string("FPackageFileSummary::FPackageFileSummary(...) : Package does not equal \"None\"!\n\t(Package Group: ").append(_packageGroup->toString()).append(")"));
	}
	input.read(_packageFlags);
	{
		uint32_t const packageFlagsValues[] = {PackageFlags::AllowDownload, PackageFlags::BrokenLinks, PackageFlags::Unknown1, PackageFlags::Script, PackageFlags::Imports, PackageFlags::FullyCompressed, PackageFlags::NoExportsData, PackageFlags::Protected};
		checkFlagsAssigned(_packageFlags, packageFlagsValues, 8, "FPackageFileSummary::FPackageFileSummary(...)", "Package");
	}
	int32_t exportCount, importCount, nameCount;
	input.read(nameCount);
	input.read(_nameOffset);
	input.read(exportCount);
	input.read(_exportOffset);
	input.read(importCount);
	input.read(_importOffset);
	input.read(_dependsOffset);
	input.read(_serializedOffset);
	if(_serializedOffset != headerSize){
		throw std::runtime_error(std::string("FPackageFileSummary::FPackageFileSummary(...) : Serial offset does not match header size!\n\t(Serialized Offset: ").append(std::to_string(_serializedOffset)).append("  Header Size: ").append(std::to_string(headerSize)).append(")"));
	}
	{
		int32_t temp;
		for(uint8_t i = 0; i < 3; i++){
			input.read(temp);
			if(temp != 0){
				std::string number = std::to_string(i + 2);
				throw std::runtime_error(std::string("FPackageFileSummary::FPackageFileSummary(...) : Unknown ").append(number).append(" does not equal 0!\n\t(Unknown ").append(number).append(": ").append(std::to_string(temp)).append(")"));
			}
		}
	}
	_guid = new FGuid(input);
	_generations = new TArray<FGenerationInfo>(input);
	input.read(_engineVersion);
	if(_engineVersion != 11841){
		throw std::runtime_error(std::string("FPackageFileSummary::FPackageFileSummary(...) : Engine version not mobile value!\n\t(Engine Version: ").append(std::to_string(_engineVersion)).append("  Mobile Engine Version: 11841)"));
	}
	input.read(_cookerVersion);
	if(_cookerVersion != 721032){
		throw std::runtime_error(std::string("FPackageFileSummary::FPackageFileSummary(...) : Cooker version not mobile value!\n\t(Cooker Version: ").append(std::to_string(_cookerVersion)).append("  Mobile Cooker Version: 721032)"));
	}
	input.read(_compressionFlags);
	{
		uint32_t const compressionFlagsValues[1] = {};
		checkFlagsAssigned(_compressionFlags, compressionFlagsValues, 0, "FPackageFileSummary::FPackageFileSummary(...)", "Compression");
	}
	_compressedChunks = new TArray<FCompressedChunk>(input);
	{
		int32_t const size = _nameOffset - input.getOffset();
		_unassigned.resize(size);
		uint8_t temp;
		for(int32_t i = 0; i < size; i++){
			input.read(temp);
			_unassigned.at(i) = temp;
		}
	}
	//TODO: List of materials?
	{
		_nameTable.resize(nameCount);
		for(int32_t i = 0; i < nameCount; i++){
			_nameTable.at(i) = new FNameEntry(input);
		}
	}
	{
		_importTable.resize(importCount);
		for(int32_t i = 0; i < importCount; i++){
			_importTable.at(i) = new FObjectImport(input, *this);
		}
	}
	{
		_exportTable.resize(exportCount);
		for(int32_t i = 0; i < exportCount; i++){
			_exportTable.at(i) = new FObjectExport(input, *this);
		}
	}
	{
		_dependsCount = headerSize - _dependsOffset;
		if(_dependsCount < 0){
			throw std::runtime_error(std::string("FPackageFileSummary::FPackageFileSummary(...) : Invalid depends table size!\n\t(Depends Count: ").append(std::to_string(_dependsCount)).append(")"));
		}
		uint8_t temp;
		for(int32_t i = 0; i < _dependsCount; i++){
			input.read(temp);
			if(temp != 0){
				int32_t offset = _dependsOffset + i;
				throw std::runtime_error(std::string("FPackageFileSummary::FPackageFileSummary(...) : Depends table contents not 0!\n\t(Offset: ").append(UPKReader::toHexString(&offset, sizeof(offset))).append(")"));
			}
		}
	}
	int32_t const temp = size();
	if(temp != headerSize){
		throw std::runtime_error(std::string("FPackageFileSummary::FPackageFileSummary(...) : Mismatch header size!\n\t(Calculated: ").append(std::to_string(temp)).append("  Written: ").append(std::to_string(headerSize)).append(")"));
	}
	for(std::vector<FObjectExport*>::const_iterator it = _exportTable.begin(); it != _exportTable.end(); it++){
		(*it)->readData(input);
	}
	for(std::vector<FObjectExport*>::const_iterator it = _exportTable.begin(); it != _exportTable.end(); it++){
		(*it)->unpackRaw();
	}
}

FPackageFileSummary::~FPackageFileSummary(){
	delete _compressedChunks;
	delete _generations;
	delete _guid;
	for(std::vector<FObjectExport*>::const_iterator it = _exportTable.begin(); it != _exportTable.end(); it++){
		delete *it;
	}
	for(std::vector<FObjectImport*>::const_iterator it = _importTable.begin(); it != _importTable.end(); it++){
		delete *it;
	}
	for(std::vector<FNameEntry*>::const_iterator it = _nameTable.begin(); it != _nameTable.end(); it++){
		delete *it;
	}
	delete _packageGroup;
}


FObjectExport* const FPackageFileSummary::getExport(int32_t const& index) const{
	return _exportTable.at(index - 1);
}
FObjectImport* const FPackageFileSummary::getImport(int32_t const& index) const{
	return _importTable.at((-index) - 1);
}

FNameEntry* const FPackageFileSummary::getName(int32_t const& index) const{
	return _nameTable.at(index);
}

uint32_t const FPackageFileSummary::size() const{
	uint32_t size = (sizeof(int32_t) * 7) + _compressedChunks->size() + sizeof(_compressionFlags) + sizeof(_cookerVersion) + _dependsCount + sizeof(_dependsOffset) + sizeof(_engineVersion) + sizeof(_exportOffset) + _generations->size() + _guid->size() + sizeof(_importOffset) + sizeof(_nameOffset) + sizeof(_packageFlags) + _packageGroup->size() + sizeof(_serializedOffset) + sizeof(_signature) + _unassigned.size() + sizeof(_version);
	for(std::vector<FObjectExport*>::const_iterator it = _exportTable.begin(); it != _exportTable.end(); it++){
		size += (*it)->size();
	}
	for(std::vector<FObjectImport*>::const_iterator it = _importTable.begin(); it != _importTable.end(); it++){
		size += (*it)->size();
	}
	for(std::vector<FNameEntry*>::const_iterator it = _nameTable.begin(); it != _nameTable.end(); it++){
		size += (*it)->size();
	}
	return size;
}


void const FPackageFileSummary::write(UPKWriter& output) const{
	if(_signature != 0x9E2A83C1){
		throw std::runtime_error(std::string("FPackageFileSummary::write(...) : Invalid signature!\n\t(Signature: ").append(UPKReader::toHexString(&_signature, sizeof(_signature))).append(")"));
	}
	output.write(_signature);
	if((_version & 0xFFFF) != 870){
		throw std::runtime_error(std::string("FPackageFileSummary::write(...) : Package version not mobile value!\n\t(Package Version: ").append(std::to_string(_version & 0xFFFF)).append("  Mobile Package Version: 870)"));
	}
	if(_version >> 16 != 64){
		throw std::runtime_error(std::string("FPackageFileSummary::write(...) : Licensee version not mobile value!\n\t(Licensee Version: ").append(std::to_string(_version >> 16)).append("  Mobile Licensee Version: 64)"));
	}
	output.write(_version);
	int32_t const headerSize = size();
	output.write(headerSize);
	if(!_packageGroup->equals("None")){
		throw std::runtime_error(std::string("FPackageFileSummary::write(...) : Package does not equal \"None\"!\n\t(Package Group: ").append(_packageGroup->toString()).append(")"));
	}
	_packageGroup->write(output);
	{
		uint32_t const packageFlagsValues[] = {PackageFlags::AllowDownload, PackageFlags::BrokenLinks, PackageFlags::Unknown1, PackageFlags::Script, PackageFlags::Imports, PackageFlags::FullyCompressed, PackageFlags::NoExportsData, PackageFlags::Protected};
		checkFlagsAssigned(_packageFlags, packageFlagsValues, 8, "FPackageFileSummary::write(...)", "Package");
	}
	output.write(_packageFlags);
	output.write((int32_t) _nameTable.size());
	output.write(_nameOffset);
	output.write((int32_t) _exportTable.size());
	output.write(_exportOffset);
	output.write((int32_t) _importTable.size());
	output.write(_importOffset);
	output.write(_dependsOffset);
	if(_serializedOffset != headerSize){
		throw std::runtime_error(std::string("FPackageFileSummary::write(...) : Serial offset does not match header size!\n\t(Serialized Offset: ").append(std::to_string(_serializedOffset)).append("  Header Size: ").append(std::to_string(headerSize)).append(")"));
	}
	output.write(_serializedOffset);
	{
		int32_t const temp = 0;
		for(uint8_t _ = 0; _ < 3; _++){
			output.write(temp);
		}
	}
	_guid->write(output);
	_generations->write(output);
	if(_engineVersion != 11841){
		throw std::runtime_error(std::string("FPackageFileSummary::write(...) : Engine version not mobile value!\n\t(Engine Version: ").append(std::to_string(_engineVersion)).append("  Mobile Engine Version: 11841)"));
	}
	output.write(_engineVersion);
	if(_cookerVersion != 721032){
		throw std::runtime_error(std::string("FPackageFileSummary::write(...) : Cooker version not mobile value!\n\t(Cooker Version: ").append(std::to_string(_cookerVersion)).append("  Mobile Cooker Version: 721032)"));
	}
	output.write(_cookerVersion);
	{
		uint32_t const compressionFlagsValues[1] = {};
		checkFlagsAssigned(_compressionFlags, compressionFlagsValues, 0, "FPackageFileSummary::write(...)", "Compression");
	}
	output.write(_compressionFlags);
	_compressedChunks->write(output);
	for(std::vector<uint8_t>::const_iterator it = _unassigned.begin(); it != _unassigned.end(); it++){
		output.write(*it);
	}
	//TODO: List of materials?
	saveTable(output, _nameTable);
	saveTable(output, _importTable);
	{
		int32_t offset = _serializedOffset;
		for(std::vector<FObjectExport*>::const_iterator it = _exportTable.begin(); it != _exportTable.end(); it++){
			(*it)->write(output, offset);
			offset += (*it)->dataSize();
		}
	}
	{
		uint8_t const temp = 0;
		for(int32_t _ = 0; _ < _dependsCount; _++){
			output.write(temp);
		}
	}
	for(std::vector<FObjectExport*>::const_iterator it = _exportTable.begin(); it != _exportTable.end(); it++){
		(*it)->writeData(output);
	}
}
void const FPackageFileSummary::write(UPKWriter& output, bool const& updateOffsets){
	if(updateOffsets){
		_nameOffset = (sizeof(int32_t) * 7) + _compressedChunks->size() + sizeof(_compressionFlags) + sizeof(_cookerVersion) + sizeof(_dependsOffset) + sizeof(_engineVersion) + sizeof(_exportOffset) + _generations->size() + _guid->size() + sizeof(_importOffset) + sizeof(_nameOffset) + sizeof(_packageFlags) + _packageGroup->size() + sizeof(_serializedOffset) + sizeof(_signature) + _unassigned.size() + sizeof(_version);
		_importOffset = _nameOffset;
		for(std::vector<FNameEntry*>::const_iterator it = _nameTable.begin(); it != _nameTable.end(); it++){
			_importOffset += (*it)->size();
		}
		_exportOffset = _importOffset;
		for(std::vector<FObjectImport*>::const_iterator it = _importTable.begin(); it != _importTable.end(); it++){
			_exportOffset += (*it)->size();
		}
		_dependsOffset = _exportOffset;
		for(std::vector<FObjectExport*>::const_iterator it = _exportTable.begin(); it != _exportTable.end(); it++){
			_dependsOffset += (*it)->size();
		}
		_serializedOffset = _dependsOffset + _dependsCount;
	}
	write(output);
}


template <class T> static void const FPackageFileSummary::saveTable(UPKWriter& output, std::vector<T*> const& table){
	for(typename std::vector<T*>::const_iterator it = table.begin(); it != table.end(); it++){
		(*it)->write(output);
	}
}