#pragma once

#include "Object.h"

class FObjectBase;


class UObjectReference : public Object{
	private:
		/*
			Index to an object.
			'_objectReference == 0' is null.
			'_objectReference > 0' is from export table.
			'_objectReference < 0' is from import table.
		*/
		int32_t _objectReference;


	public:
		/*
			Reference to the null object.
		*/
		static UObjectReference const NULL_REFERENCE;


		/*
			Creates an object reference from data.

			\param 'index' Index of an object reference.
		*/
		UObjectReference(int32_t const& index);

		/*
			Creates an object reference from a UPK file.

			\param 'input' Input UPK file reader.
		*/
		UObjectReference(UPKReader& input);


		/*
			Gets the object that is referenced.

			\param 'summary' Summary of UPK file.

			\return Object that is referenced.
		*/
		FObjectBase* const object(FPackageFileSummary const& summary) const;

		uint32_t const size() const override;


		/*
			Checks if the object reference is equal to another object reference.

			\param 'o' Object reference to check.

			\return 'true' if object references are equal.
		*/
		bool const equals(UObjectReference const& o) const;


		void const write(UPKWriter& output) const override;
};