#pragma once

#include "summary/FCompressedChunk.h"
#include "summary/FGenerationInfo.h"
#include "types/FGuid.h"
#include "summary/FNameEntry.h"
#include "summary/FObjectExport.h"
#include "summary/FObjectImport.h"
#include "types/TArray.h"


class FPackageFileSummary : public Object{
	public:
		enum PackageFlags: uint32_t{
			AllowDownload	= 0x00000001, // Allow downloading package
			BrokenLinks		= 0x00000008, // Loaded from linker with broken import links
			Unknown1		= 0x00080000, // TODO:  Unknown?
			Script			= 0x00200000, // TODO: Script?
			Imports			= 0x00800000, // TODO: Imports?
			FullyCompressed	= 0x04000000, // TODO: Fully compressed?
			NoExportsData	= 0x20000000, // TODO: No exports data?
			Protected		= 0x80000000 // TODO: Protected?
		};


	private:
		/*
			Table of index labels to export entries.
		*/
		std::vector<FObjectExport*> _exportTable;

		/*
			Table of index labels to import entries.
		*/
		std::vector<FObjectImport*> _importTable;

		/*
			Table of index labels to name entries.
		*/
		std::vector<FNameEntry*> _nameTable;


		/*
			Chunks of compressed data.
		*/
		TArray<FCompressedChunk>* _compressedChunks;
		/*
			Indicator for compression methods used.
		*/
		int32_t _compressionFlags;

		/*
			Version of the cooking tools.
		*/
		int32_t _cookerVersion;

		/*
			Number of depends table 0 bytes.
		*/
		int32_t _dependsCount;
		/*
			Offset in file to find depends table data.
		*/
		int32_t _dependsOffset;

		/*
			Version of the engine.
		*/
		int32_t _engineVersion;

		/*
			Offset in file to find export table data.
		*/
		int32_t _exportOffset;

		/*
			Record of compilation history.
		*/
		TArray<FGenerationInfo>* _generations;

		/*
			Global unique identifier for the package.
		*/
		FGuid* _guid;

		/*
			Offset in file to find import table offset.
		*/
		int32_t _importOffset;

		/*
			Offset in file to find name table data.
		*/
		int32_t _nameOffset;

		/*
			Name of the package this resides in.
		*/
		FString* _packageGroup;
		/*
			Indicator for properties of the package.
		*/
		int32_t _packageFlags;

		/*
			Offset in file to find serialized data.
		*/
		int32_t _serializedOffset;

		/*
			Signature of the package.
		*/
		int32_t _signature;

		/*
			TODO: Unassigned values between header and name table?
		*/
		std::vector<uint8_t> _unassigned;

		/*
			'_version & 0xFFFF' is package version.
			'_version >> 16' is licensee version.
		*/
		int32_t _version;


		/*
			Writes table entries from table into UPK file.

			\param 'input' Output UPK file writer.
			\param 'table' Table to save.
		*/
		template <class T> static void const saveTable(UPKWriter& output, std::vector<T*> const& table);


	public:
		/*
			Creates a summary of package contents.

			\param 'input' Input UPK file reader.
		*/
		FPackageFileSummary(UPKReader& input);

		~FPackageFileSummary();


		/*
			Gets an object from the export table.

			\param 'index' Index of the object.

			\return Export object entry.
		*/
		FObjectExport* const getExport(int32_t const& index) const;
		/*
			Gets an object from the import table.

			\param 'index' Index of the object.

			\return Import object entry.
		*/
		FObjectImport* const getImport(int32_t const& index) const;

		/*
			Gets a name entry from the name table.

			\param 'index' Index of the name entry.

			\return Name entry.
		*/
		FNameEntry* const getName(int32_t const& index) const;

		uint32_t const size() const override;


		void const write(UPKWriter& output) const override;
		/*
			Writes summary of package contents data to UPK file with new offsets.

			\param 'output' Output UPK file writer.
			\param 'updateOffsets' 'true' if offsets should be updated for new data.
		*/
		void const write(UPKWriter& output, bool const& updateOffsets);
};