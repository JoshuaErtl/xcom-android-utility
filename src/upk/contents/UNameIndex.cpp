//#include "UNameIndex.h"

#include <string>

#include "FPackageFileSummary.h"


UNameIndex::UNameIndex(UPKReader& input){
	input.read(_nameTableIndex);
	input.read(_numeric);
}


uint32_t const UNameIndex::size() const{
	return sizeof(_nameTableIndex) + sizeof(_numeric);
}


std::string UNameIndex::toString(FPackageFileSummary const& summary) const{
	std::string ret = summary.getName(_nameTableIndex)->toString();
	if(_numeric > 0){
		ret.append("_").append(std::to_string(_numeric - 1));
	}
	return ret;
}

void const UNameIndex::write(UPKWriter& output) const{
	output.write(_nameTableIndex);
	output.write(_numeric);
}