#include "UPKReader.h"

#include <iomanip>
#include <sstream>


void const UPKReader::read(float& value){
	read(&value, 4);
}

void const UPKReader::read(int8_t& value){
	read(&value, 1);
}
void const UPKReader::read(uint8_t& value){
	read(&value, 1);
}

void const UPKReader::read(int16_t& value){
	read(&value, 2);
}
void const UPKReader::read(uint16_t& value){
	read(&value, 2);
}

void const UPKReader::read(int32_t& value){
	read(&value, 4);
}
void const UPKReader::read(uint32_t& value){
	read(&value, 4);
}

void const UPKReader::read(int64_t& value){
	read(&value, 8);
}

char const* const UPKReader::readString(int32_t const& length){
	char* const ret = new char[length];
	if(length != 0){
		if(length > 0){
			read(ret, length);
		}
		else{
			throw std::runtime_error(std::string("UPKReader::readString(").append(std::to_string(length)).append(") : Unicode string not handled!"));
		}
	}
	return ret;
}


std::string const UPKReader::toHexString(void const* const& source, std::streamsize const& size, bool const& prefix){
	std::stringstream str;
	if(prefix){
		str << "0x";
	}
	str << std::hex << std::uppercase;
	uint_least8_t* src = (uint_least8_t*) source;
	for(std::streamsize i = 1; i <= size; i++){
		str << std::setfill('0') << std::setw(2) << (uint_least16_t) src[size - i];
	}
	return str.str();
}