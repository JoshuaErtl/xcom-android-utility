This utility will be an all-purpose package for unpacking, decompiling, and modifying the Android
(and potentially the PC version if time allows) version of the Unreal package files (UPKs) found in
the game XCOM: Enemy Within.
https://play.google.com/store/apps/details?id=com.tt2kgames.xcomew

While tools exist somewhat for the mobile release that were derived from the existing and popular
desktop version, I figured I would make my attempt at fully unpacking these different UPK files
using only the documentation provided for the PC releases.  Essentially, this repository ignores
any and all existing source code from other tools and is simply my personal study and findings
utilizing only the public PC documentation (Primarily found below, but not limited to):

http://wiki.tesnexus.com/index.php/UPK_File_Format_-_XCOM:EU_2012
https://forums.nexusmods.com/index.php?/topic/1254328-upk-file-format/

This utility will attempt to match as closely to the naming conventions already set by other tools,
will attempt to match in function to other tools, but will in no way be influenced or utilize
existing code from these tools (excluding that which Unreal uses itself, such as compression
methods or packing techniques).

This utility will additionally attempt to fully repack these UPK files through means of reading raw
source code that was originally output by this same utility, a function that currently does not
exist for even the desktop platform (at least does not exist to my knowledge).
Hopefully by the end of this project there will be an overarching method to unpack the unmodified
game assets, convert the packaged files to a source code styled file structure, modify files
through manual source edits or automated patch files, and finally repack from memory or files into
acceptable UPK files and ultimately the required OBB Android asset archive.

Current Status:  Attempting data dump output